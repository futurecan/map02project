package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_ItemsDefault {

    @Insert
    void insertItemsDefault (Obj_ItemsDefault itemsDefault);

    @Insert
    void insertMultipleItemsDefault (List<Obj_ItemsDefault> itemsDefaultList);

    @Query("SELECT * FROM itemsDefault WHERE itemId = :itemId ")
    Obj_ItemsDefault fetchItemsDefaultById(int itemId);

    @Query("SELECT * FROM itemsDefault")
    List<Obj_ItemsDefault> fetchAllItemsDefault();

    @Update
    void updateItemsDefault (Obj_ItemsDefault itemsDefault);

    @Delete
    void deleteItemsDefault (Obj_ItemsDefault itemsDefault);
}
