package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "categoriesUsers")

public class Obj_CategoryUsers {

    @PrimaryKey (autoGenerate = true)

    int catId ;
    String catName ;
    int catAdminId ;

    public Obj_CategoryUsers(int catId, String catName, int catAdminId) {
        this.catId = catId;
        this.catName = catName;
        this.catAdminId = catAdminId;
    }

    @Override
    public String toString() {
        return String.format(catName );
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatAdminId() {
        return catAdminId;
    }

    public void setCatAdminId(int catAdminId) {
        this.catAdminId = catAdminId;
    }
}
