package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "notifications")


public class Obj_Notifications {

    @PrimaryKey(autoGenerate = true)
    int notifId ;
    int notifToUser ;
    int notifFromUser ;
    String notifType  ;
    String notifStatus ;
    int notifTypeId ;

    public Obj_Notifications(int notifId, int notifToUser, int notifFromUser, String notifType, String notifStatus, int notifTypeId) {
        this.notifId = notifId;
        this.notifToUser = notifToUser;
        this.notifFromUser = notifFromUser;
        this.notifType = notifType;
        this.notifStatus = notifStatus;
        this.notifTypeId = notifTypeId;
    }

    public int getNotifId() {
        return notifId;
    }

    public void setNotifId(int notifId) {
        this.notifId = notifId;
    }

    public int getNotifToUser() {
        return notifToUser;
    }

    public void setNotifToUser(int notifToUser) {
        this.notifToUser = notifToUser;
    }

    public int getNotifFromUser() {
        return notifFromUser;
    }

    public void setNotifFromUser(int notifFromUser) {
        this.notifFromUser = notifFromUser;
    }

    public String getNotifType() {
        return notifType;
    }

    public void setNotifType(String notifType) {
        this.notifType = notifType;
    }

    public String getNotifStatus() {
        return notifStatus;
    }

    public void setNotifStatus(String notifStatus) {
        this.notifStatus = notifStatus;
    }

    public int getNotifTypeId() {
        return notifTypeId;
    }

    public void setNotifTypeId(int notifTypeId) {
        this.notifTypeId = notifTypeId;
    }

    @Override
    public String toString() {
        return String.format(notifId + " | " +  notifToUser + " | " +  notifFromUser + " | " +  notifType + " | " +  notifStatus + " | " + notifTypeId);
    }
}
