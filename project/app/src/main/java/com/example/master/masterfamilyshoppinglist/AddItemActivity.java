package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddItemActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ImageView iv_reduceItem ;
    ImageView iv_increaseItem ;
    TextView tv_unitsCount ;
    EditText et_unitPrice ;
    TextView tv_totalPrice ;
    EditText et_itemName ;
    EditText et_itemNotes ;
    Spinner sp_category ;
    ImageView iv_category ;
    Spinner sp_store ;
    ImageView iv_store ;
    Button bt_addToList ;

    public int myCount ;
    int categoryId , storeId , listId;

    List<Obj_CategoryUsers  > categoryList;
    ArrayAdapter<Obj_CategoryUsers> categoryAdapter;
    List<Obj_Stores  > storeList;
    ArrayAdapter<Obj_Stores> storeAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        iv_reduceItem = (ImageView) findViewById(R.id.iv_reduceUnit) ;
        iv_increaseItem = (ImageView) findViewById(R.id.iv_addUnits) ;
        tv_unitsCount = (TextView) findViewById(R.id.tv_unitsCount) ;
        et_unitPrice = (EditText) findViewById(R.id.et_unitPrice) ;
        tv_totalPrice = (TextView) findViewById(R.id.tv_totalPrice) ;
        et_itemName = (EditText) findViewById(R.id.et_itemName) ;
        et_itemNotes = (EditText) findViewById(R.id.et_itemNotes) ;
        sp_category = (Spinner) findViewById(R.id.sp_category) ;
        iv_category = (ImageView) findViewById(R.id.iv_category) ;
        sp_store = (Spinner) findViewById(R.id.sp_store) ;
        iv_store = (ImageView) findViewById(R.id.iv_store) ;
        bt_addToList = (Button) findViewById(R.id.bt_addToList);



        //et_unitPrice.setText(mCrime.getTitle());

        et_unitPrice.addTextChangedListener(new TextWatcher() {

            // the user's changes are saved here
            public void onTextChanged(CharSequence c, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                calcTotal();
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // this space intentionally left blank
            }


        });

        iv_reduceItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 myCount = Integer.parseInt(tv_unitsCount.getText().toString())  ;
                 if (myCount > 0 ) {
                     myCount -- ;
                     calcTotal();

                }
               tv_unitsCount.setText(String.valueOf(myCount));
            }
        });

        iv_increaseItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 myCount = Integer.parseInt(tv_unitsCount.getText().toString())  ;
                 myCount ++ ;
                 tv_unitsCount.setText(String.valueOf(myCount));
                 calcTotal();
            }
        });

        iv_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddItemActivity.this, AddCategoryActivity.class);
                startActivity(intent);
            }
        });

        iv_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddItemActivity.this, AddStoreActivity.class);
                startActivity(intent);
            }
        });

        bt_addToList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = et_itemName.getText().toString() ;
                String note = et_itemNotes.getText().toString();
                int quantity = Integer.parseInt(tv_unitsCount.getText().toString());
                String price = tv_totalPrice.getText().toString();
                listId=1;
                Obj_ItemsUsers myItem = new Obj_ItemsUsers(Z_Globals.MYID , 0 , name ,
                        note , categoryId , listId,"pending","",price,storeId ,Z_Globals.MYID,quantity);
                new InsertItemDbAsyncTask().execute(myItem) ;

            }
        });



        //***********************************************************
        //             LOAD CATEGORIES INTO SPINNER
        //***********************************************************
        new LoadCategoryFromDbAsyncTask().execute();

        categoryList = new ArrayList<>();
        categoryAdapter = new ArrayAdapter<Obj_CategoryUsers>( AddItemActivity.this, android.R.layout.simple_list_item_1, categoryList);
        sp_category.setAdapter(categoryAdapter);

        //***********************************************************
        //             Handel Select item from  SPINNER
        //***********************************************************
        sp_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Obj_CategoryUsers cat = (Obj_CategoryUsers)parent.getItemAtPosition(position);
                categoryId = cat.catId; //this would give you the id of the selected item
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        //***********************************************************
        //             Handel Select item from  SPINNER
        //***********************************************************
        sp_store.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Obj_Stores store = (Obj_Stores)parent.getItemAtPosition(position);
                storeId = store.storeId; //this would give you the id of the selected item
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        //***********************************************************
        //             LOAD Stores INTO SPINNER
        //***********************************************************
        new LoadCStoreFromDbAsyncTask().execute();
        //Creating the ArrayAdapter instance having the country list
        storeList = new ArrayList<>();
        storeAdapter = new ArrayAdapter<Obj_Stores>( AddItemActivity.this, android.R.layout.simple_list_item_1, storeList);
        sp_store.setAdapter(storeAdapter);
    }



    void calcTotal(){
        try {
            double unitprice = Double.parseDouble(et_unitPrice.getText().toString() );
            int count =Integer.parseInt(tv_unitsCount.getText().toString());
            double total =  unitprice * count;
            tv_totalPrice.setText(String.valueOf(total) );
        }catch( Exception e){
            Toast.makeText(this,"Calculation has not succeed please verify values",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //String id = sp_category.get(position).getId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this,"Please select Category",Toast.LENGTH_LONG).show();
    }



    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadCategoryFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_CategoryUsers>> {

        @Override
        protected List<Obj_CategoryUsers> doInBackground(Void... params) {
            List<Obj_CategoryUsers> list = Z_Globals.db.dao_categoryUsers().fetchAllCategoryUsers();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_CategoryUsers> list) {
            if (list == null) {
                Toast.makeText(AddItemActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                categoryList.clear();
                for (Obj_CategoryUsers category : list) {
                    categoryList.add(category);
                }
                categoryAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadCStoreFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_Stores>> {

        @Override
        protected List<Obj_Stores> doInBackground(Void... params) {
            List<Obj_Stores> list = Z_Globals.db.dao_stores().fetchAllStores();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_Stores> list) {
            if (list == null) {
                Toast.makeText(AddItemActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                storeList.clear();
                for (Obj_Stores store : list) {
                    storeList.add(store);
                }
                storeAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//

    class InsertItemDbAsyncTask extends AsyncTask<Obj_ItemsUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_ItemsUsers... params) {
            Obj_ItemsUsers list = params[0];
            Z_Globals.db.dao_itemsUsers().insertItemsUsers(list);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            Toast.makeText(AddItemActivity.this, "Item is added " , Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        new LoadCategoryFromDbAsyncTask().execute() ;
        new LoadCStoreFromDbAsyncTask().execute() ;
    }
}
