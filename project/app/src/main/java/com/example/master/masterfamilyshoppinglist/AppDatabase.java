package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Obj_CategoryUsers.class , Obj_Circles.class ,
        Obj_CircleUsers.class , Obj_Friends.class ,
        Obj_ItemCategoriesDefault.class , Obj_ItemsDefault.class ,
        Obj_ItemsUsers.class , Obj_ListExchange.class ,
        Obj_ListsUsers.class , Obj_Notifications.class ,
        Obj_Stores.class , Obj_Users.class }, version = 1, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {


    public abstract Dao_CategoryUsers dao_categoryUsers ();
    public abstract Dao_Circles dao_circles ();
    public abstract Dao_CircleUsers dao_circleUsers ();
    public abstract Dao_Friends dao_friends ();
    public abstract Dao_ItemCategoriesDefault dao_itemCategoriesDefault ();
    public abstract Dao_ItemsDefault dao_itemsDefault ();
    public abstract Dao_ItemsUsers dao_itemsUsers ();
    public abstract Dao_ListExchange dao_listExchange ();
    public abstract Dao_ListsUsers dao_listsUsers ();
    public abstract Dao_Notifications dao_notifications ();
    public abstract Dao_Stores dao_stores ();
    public abstract Dao_Users dao_users ();
}
