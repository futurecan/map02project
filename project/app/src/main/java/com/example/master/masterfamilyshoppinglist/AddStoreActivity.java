package com.example.master.masterfamilyshoppinglist;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddStoreActivity extends AppCompatActivity {

    private android.support.v7.widget.Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    EditText et_storeName ;
    Button bt_addStore ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        //ToolBar
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Stores");

        et_storeName = (EditText) findViewById(R.id.et_storeName);
        bt_addStore = (Button) findViewById(R.id.bt_addStore) ;


        bt_addStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String store = et_storeName.getText().toString() ;
                new InsertStoreDbAsyncTask().execute(new Obj_Stores(0 , store)) ;

                Toast.makeText(AddStoreActivity.this, "Store is added " , Toast.LENGTH_SHORT).show();
            }
        });

    }

    class InsertStoreDbAsyncTask extends AsyncTask<Obj_Stores, Void, Void> {

        @Override
        protected Void doInBackground(Obj_Stores... params) {
            Obj_Stores store = params[0];
            Z_Globals.db.dao_stores().insertStores(store);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
           // Toast
           finish();
        }

    }

}
