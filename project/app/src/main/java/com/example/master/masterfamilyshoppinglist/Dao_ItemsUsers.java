package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_ItemsUsers {


    @Insert
    void insertItemsUsers (Obj_ItemsUsers itemsUsers);

    @Insert
    void insertMultipleItemsUsers  (List<Obj_ItemsUsers> itemsUsersList);

    @Query("SELECT * FROM `itemsUsers` WHERE  itemStatus='pending' and listId=:listId")
    List<Obj_ItemsUsers>  fetchItemsItemsUsersByList (int listId);

    @Query("SELECT * FROM itemsUsers WHERE itemUserid = :itemUserId ")
    Obj_ItemsUsers fetchItemsItemsUsersById(int itemUserId);

    @Query("SELECT * FROM itemsUsers where listId = :listId")
    List<Obj_ItemsUsers> fetchAllItemsUsers (int listId);

    @Query("update itemsUsers set itemStatus = 'done' where itemId = :itemId")
    void markItemDone (int itemId);

    @Update
    void updateItemsUsers (Obj_ItemsUsers itemsUsers);

    @Delete
    void deleteItemsUsers (Obj_ItemsUsers itemsUsers);
}
