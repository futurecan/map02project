package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_CircleUsers {

    @Insert
    void insertCircleUsers (Obj_CircleUsers circleUsers);

    @Insert
    void insertMultipleCircleUsers (List<Obj_CircleUsers> circleUsersList);

    @Query("SELECT * FROM CircleUsers WHERE circleUsersId = :circleUsersId")
    Obj_CircleUsers fetchCircleUsersById (int circleUsersId);

    @Query("SELECT * FROM circleUsers")
    List<Obj_CircleUsers> fetchAllCircleUsers ();

    @Update
    void updateCircleUsers (Obj_CircleUsers circleUsers);

    @Delete
    void deleteCircleUsers (Obj_CircleUsers circleUsers);

}
