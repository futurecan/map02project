package com.example.master.masterfamilyshoppinglist;

import android.os.AsyncTask;

public class LoadDefault {

    String catName ;
    int adminId ;

    String storeName ;

    public LoadDefault() {
        loadAction();
    }

    public LoadDefault(String catName, int adminId, String storeName) {
        this.catName = catName;
        this.adminId = adminId;
        this.storeName = storeName;
    }

    public void loadAction(){
        adminId = Z_Globals.MYID;
        catName = "Default Category";
        storeName="Default Store";

           new InsertCatDbAsyncTask().execute( new Obj_CategoryUsers(1, catName , adminId));
           new InsertStoreDbAsyncTask().execute(new Obj_Stores(1 , storeName)) ;

        }


    class InsertCatDbAsyncTask extends AsyncTask<Obj_CategoryUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_CategoryUsers... params) {
            Obj_CategoryUsers category = params[0];
            try {
                Z_Globals.db.dao_categoryUsers().insertCategoryUsers(category);
            }catch(Exception e){

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void list) {

        }

    }
    class InsertStoreDbAsyncTask extends AsyncTask<Obj_Stores, Void, Void> {

        @Override
        protected Void doInBackground(Obj_Stores... params) {
            Obj_Stores store = params[0];
            try {
            Z_Globals.db.dao_stores().insertStores(store);
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {

        }

    }
}
