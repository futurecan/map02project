package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "listsUsers")

public class Obj_ListsUsers {

    @PrimaryKey(autoGenerate = true)
    int listId;
    int listAdmin ;
    String listName ;
    String listNote ;
    String listActive ;
    String listSaved ;

    public Obj_ListsUsers(int listId, int listAdmin, String listName, String listNote, String listActive , String listSaved ) {
        this.listId = listId;
        this.listAdmin = listAdmin;
        this.listName = listName;
        this.listNote = listNote;
        this.listActive = listActive;
        this.listSaved = listSaved;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getListAdmin() {
        return listAdmin;
    }

    public void setListAdmin(int listAdmin) {
        this.listAdmin = listAdmin;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListNote() {
        return listNote;
    }

    public void setListNote(String listNote) {
        this.listNote = listNote;
    }

    public String getListActive() {
        return listActive;
    }

    public void setListActive(String listActive) {
        this.listActive = listActive;
    }

    public String getListSaved() {
        return listSaved;
    }

    public void setListSaved(String listSaved) {
        this.listSaved = listSaved;
    }

    @Override
    public String toString() {
        return String.format(listId + " | " + listAdmin + " | " + listName + " | " +  listNote + " | " +  listActive + " | " +  listSaved);
    }
}
