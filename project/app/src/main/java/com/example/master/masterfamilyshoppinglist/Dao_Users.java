package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_Users {

    @Insert
    void insertUsers (Obj_Users users);

    @Insert
    void insertMultipleUsers (List<Obj_Users> usersList);

    @Query("SELECT * FROM users WHERE userId = :userId")
    Obj_Users fetchUsersById(int userId);

    @Query("SELECT * FROM users")
    List<Obj_Users> fetchAllUsers();

    @Update
    void updateUsers (Obj_Users users);

    @Delete
    void deleteUsers (Obj_Users users);
}
