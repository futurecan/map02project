package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "circles")

public class Obj_Circles {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("circleId")
    int circleId ;
    @SerializedName("circleName")
    String circleName ;
    @SerializedName("circleAdminId")
    int circleAdminId ;
    @SerializedName("circleDescription")
    String circleDescription ;
    @SerializedName("circleStatus")
    String circleStatus  ;
   // enum CircleStatus {active , disabled} ;

    public Obj_Circles(int circleId, String circleName, int circleAdminId, String circleDescription, String circleStatus ) {
        this.circleId = circleId;
        this.circleName = circleName;
        this.circleAdminId = circleAdminId;
        this.circleDescription = circleDescription;
        this.circleStatus = circleStatus;
    }



    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public int getCircleAdminId() {
        return circleAdminId;
    }

    public void setCircleAdminId(int circleAdminId) {
        this.circleAdminId = circleAdminId;
    }

    public String getCircleDescription() {
        return circleDescription;
    }

    public void setCircleDescription(String circleDescription) {
        this.circleDescription = circleDescription;
    }


    @Override
    public String toString() {
        return
                circleName ;
    }

    public String getCircleStatus() {
        return circleStatus;
    }

    public void setCircleStatus(String circleStatus) {
        this.circleStatus = circleStatus;
    }

}
