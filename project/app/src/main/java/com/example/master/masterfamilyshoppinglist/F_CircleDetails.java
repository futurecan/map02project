package com.example.master.masterfamilyshoppinglist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class F_CircleDetails extends Fragment {



    TextView tv_circleName ;
    TextView tv_circleDescription ;
    ListView lv_CirclesList ;
    FloatingActionButton fabInviteUser  ;

    List<Obj_CircleUsers> circleUsersList ;
    ArrayAdapter<Obj_CircleUsers> circleUsersAdapter ;
    public Obj_CircleUsers selectedcircleUser ;


    public F_CircleDetails() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_circles_details_view, container, false);

        tv_circleName = (TextView) view.findViewById(R.id.tv_circleName) ;
        tv_circleDescription = (TextView) view.findViewById(R.id.tv_circleDescription) ;
        lv_CirclesList = (ListView) view.findViewById(R.id.lv_CirclesList);

        Bundle bundle= this.getArguments();
        String circleName = bundle.getString("circle name") ;
        String circleDesc = bundle.getString("circle description");
        int circleId = bundle.getInt("circleId");
        tv_circleName.setText(circleName);
        tv_circleDescription.setText(circleDesc);


        circleUsersList = new ArrayList<>();
       // circleUsersAdapter = new circleUsersArrayAdapter (this.getContext(), circleUsersList);

        lv_CirclesList.setAdapter(circleUsersAdapter);
        lv_CirclesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // String nameClicked = (String) parent.getItemAtPosition(position);
                Obj_CircleUsers onClick = (Obj_CircleUsers) parent.getItemAtPosition(position);
                showDeleteDialogBox (onClick);
                return true;
            }
        });

        //--------------------------------------------------------------------------------------------//
        /////////////*************  Floating Button to add new Record   ***************////////////
        //--------------------------------------------------------------------------------------------//
        fabInviteUser = (FloatingActionButton) view.findViewById(R.id.fabInviteUser);
        fabInviteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddItemActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        fabInviteUser.hide();
    }

    @Override
    public void onStart() {
        super.onStart();
        fabInviteUser.show();
        final int listId = this.getArguments().getInt("listId");
        new LoadCircleUsersFromDbAsyncTask().execute(listId);
    }
    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadCircleUsersFromDbAsyncTask extends AsyncTask<Integer, Void  , List<Obj_CircleUsers> > {


        @Override
        protected List<Obj_CircleUsers> doInBackground(Integer... params) {
            int listId = params [0];
            List<Obj_CircleUsers> list = (List<Obj_CircleUsers>) Z_Globals.db.dao_circleUsers().fetchCircleUsersById(listId);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_CircleUsers> list) {
            if (list == null) {
                Toast.makeText(getActivity(), "No items to show , please create items", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                circleUsersList.clear();
                for (Obj_CircleUsers myLists : list) {
                    circleUsersList.add(myLists);
                }
                circleUsersAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//


    //--------------------------------------------------------------------------------------------//
    /////////////*************  Remove from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class RemoveCircleUsersDbAsyncTask extends AsyncTask<Obj_CircleUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_CircleUsers... params) {
            Obj_CircleUsers item = params[0];
            Z_Globals.db.dao_circleUsers().deleteCircleUsers(item);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            new LoadCircleUsersFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(F_CircleDetails.this.getActivity(), "Item Deleted ", Toast.LENGTH_LONG).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Show Dialog Box to Confirm Removing   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void showDeleteDialogBox(final Obj_CircleUsers item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm Deleting Shopping List ");
        // setup the additional edit text
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new RemoveCircleUsersDbAsyncTask().execute(item);
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }
}
