package com.example.master.masterfamilyshoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "MyApp_Settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Reading from SharedPreferences to get token
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                String myToken = settings.getString("Token", "");
                Z_Globals.TOKEN = myToken;
                Z_Globals.MYID= settings.getInt("ID", 0);
                Z_Globals.userName =settings.getString("USERNAME", "");
                Z_Globals.userEmail =settings.getString("USEREMAIL", "");

                Intent intent;
                if (myToken==""){
                    intent= new Intent(MainActivity.this,LoginActivity.class);
                }else{
                    Z_Globals.DATABASE_NAME =  myToken;
                    intent = new Intent(MainActivity.this,HomeActivity.class);
                }
                startActivity(intent);

                MainActivity.this.finish();
            }
        }).start();





    }
}
