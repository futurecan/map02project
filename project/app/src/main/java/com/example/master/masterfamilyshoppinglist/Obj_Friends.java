package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "friends")


public class Obj_Friends {

    @PrimaryKey(autoGenerate = true)
    int connectId ;
    int userAId ;
    int userBId ;

    public Obj_Friends(int connectId, int userAId, int userBId) {
        this.connectId = connectId;
        this.userAId = userAId;
        this.userBId = userBId;
    }


    public int getConnectId() {
        return connectId;
    }

    public void setConnectId(int connectId) {
        this.connectId = connectId;
    }

    public int getUserAId() {
        return userAId;
    }

    public void setUserAId(int userAId) {
        this.userAId = userAId;
    }

    public int getUserBId() {
        return userBId;
    }

    public void setUserBId(int userBId) {
        this.userBId = userBId;
    }

    @Override
    public String toString() {
        return String.format( connectId + " | " + userAId + " | " +  userBId);
    }
}

