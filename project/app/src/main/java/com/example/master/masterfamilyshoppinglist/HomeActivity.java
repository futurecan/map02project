package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.File;


public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView = null;
    TextView username ;
    TextView email;
    private android.support.v7.widget.Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

  //  FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        initializing();


    }
    public void initializing (){
        //ToolBar
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // set the fragment initially
        Z_MainFragment fragment = new Z_MainFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction() ;
        fragmentTransaction.replace(R.id.fragment_container, fragment) ;
        fragmentTransaction.commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView =  navigationView.getHeaderView(0);
        navigationView.getMenu().getItem(0).setChecked(true);
        setTitle("Home");
        //********************************************************************//
        //                  Create Databse                                   //
        //********************************************************************//

        Z_Globals.db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, Z_Globals.DATABASE_NAME).build();

        //********************************************************************//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //View headerView = navigationView.getHeaderView(0);
       // headerView= navigationView.inflateHeaderView(R.layout.nav_header_navigation);

        username = (TextView)headerView.findViewById(R.id.username);
        email = (TextView)headerView.findViewById(R.id.email);
        String name = Z_Globals.userName;
        String mail = Z_Globals.userEmail;

        username.setText(name);
        email.setText(mail);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.navigation, menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.backhome) {
            Z_MainFragment fragment = new Z_MainFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
            navigationView.getMenu().getItem(0).setChecked(true);
            setTitle("Home");           return true;
       }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

   //     fab.hide();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Z_MainFragment fragment = new Z_MainFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
            setTitle("Home");
        }else if (id == R.id.nav_lists) {

            F_ShoppingListsView fragment = new F_ShoppingListsView();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

            setTitle("Shopping Lists");

        }else if (id == R.id.nav_stores) {

            F_StoreViewList fragment = new F_StoreViewList();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

            setTitle("Stores");


        } else if (id == R.id.nav_categories) {

            F_CategoriesViewList fragment = new F_CategoriesViewList();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

            setTitle("Shopping Categories");

        } else if (id == R.id.nav_circles) {

            F_CirclesViewList fragment = new F_CirclesViewList();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

            setTitle("Circles");

        } else if (id == R.id.nav_friends) {

            F_FriendsListView fragment = new F_FriendsListView();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

            setTitle("Friends");

        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(HomeActivity.this,SpinnerTest.class);
            startActivity(intent);
        } else if (id == R.id.nav_logOff) {
            //

            //************************************************************************************//
            //                               Delete Shared Preferences file
            //                                           LogOff
            //************************************************************************************//

            File deletePrefFile = new File
                    ("/data/data/com.example.master.masterfamilyshoppinglist/shared_prefs/MyApp_Settings.xml");
            deletePrefFile.delete();
            System.exit(1);




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadDefault();

    }
}
