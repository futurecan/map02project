package com.example.master.masterfamilyshoppinglist;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao

public interface Dao_Circles {

    @Insert
    void insertCircles (Obj_Circles circles);

    @Insert
    void insertMultipleCircles (List<Obj_Circles> circlesList);

    @Query("SELECT * FROM circles WHERE circleId = :circleId")
    Obj_Circles fetchCirclesById (int circleId);

    @Query("SELECT * FROM circles")
    List<Obj_Circles> fetchAllCircles ();

    @Update
    void updateCircles (Obj_Circles circles);

    @Delete
    void deleteCircles (Obj_Circles circles);

}
