package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class F_CategoriesViewList extends Fragment {

    private static final String DATABASE_NAME = "friends_db";
    private AppDatabase appDatabase;
    public final static String TAG = "CategoriesFragment";



    FloatingActionButton fabCategories  ;

    ListView lv_categoriesList ;
    List<Obj_CategoryUsers  > categoryList;
    ArrayAdapter<Obj_CategoryUsers> categoryAdapter;


    public F_CategoriesViewList() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_categories_view_list, container, false);

        lv_categoriesList = (ListView) view.findViewById(R.id.lv_categoriesList);
        categoryList = new ArrayList<>();
        categoryAdapter = new ArrayAdapter<Obj_CategoryUsers>(getActivity(), android.R.layout.simple_list_item_1, categoryList);
        lv_categoriesList.setAdapter(categoryAdapter);
        lv_categoriesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // String nameClicked = (String) parent.getItemAtPosition(position);
                Obj_CategoryUsers todoClicked = (Obj_CategoryUsers) parent.getItemAtPosition(position);
                showDeleteDialogBox (todoClicked); ;
                return true;
            }
        });

        //--------------------------------------------------------------------------------------------//
        /////////////*************  Floating Button to add new Record   ***************////////////
        //--------------------------------------------------------------------------------------------//
        fabCategories= (FloatingActionButton) view.findViewById(R.id.fabCategoriesList);
        fabCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity() , AddCategoryActivity.class);
                startActivity(intent);
                //
                appDatabase = Room.databaseBuilder(getActivity() , AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();
                //
                lv_categoriesList = (ListView) view.findViewById(R.id.lv_categoriesList);

            }
        });

        return view;


    }

    @Override
    public void onStop() {
        super.onStop();
        fabCategories.hide();
    }

    @Override
    public void onStart() {
        super.onStart();
        fabCategories.show();
        new LoadCategoryFromDbAsyncTask().execute() ;
    }

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadCategoryFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_CategoryUsers>> {

        @Override
        protected List<Obj_CategoryUsers> doInBackground(Void... params) {
            List<Obj_CategoryUsers> list = Z_Globals.db.dao_categoryUsers().fetchAllCategoryUsers();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_CategoryUsers> list) {
            if (list == null) {
                Toast.makeText(getActivity(), "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                categoryList.clear();
                for (Obj_CategoryUsers category : list) {
                    categoryList.add(category);
                }
                categoryAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//


    //--------------------------------------------------------------------------------------------//
    /////////////*************  Remove from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class RemoveCatDbAsyncTask extends AsyncTask<Obj_CategoryUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_CategoryUsers... params) {
            Obj_CategoryUsers category = params[0];
            Z_Globals.db.dao_categoryUsers().deleteCategoryUsers(category);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }
        @Override
        protected void onPostExecute(Void list) {
            new LoadCategoryFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(F_CategoriesViewList.this.getActivity(), "Category Deleted ", Toast.LENGTH_LONG).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Show Dialog Box to Confirm Removing   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void showDeleteDialogBox (final Obj_CategoryUsers category) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm Deleting Category ");
        // setup the additional edit text
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new RemoveCatDbAsyncTask().execute(category);
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }}
