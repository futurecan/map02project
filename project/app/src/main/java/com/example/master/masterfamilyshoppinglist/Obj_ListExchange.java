package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "listExchange")

public class Obj_ListExchange {

    @PrimaryKey(autoGenerate = true)
    int listId ;
    int listIncoming ;
    int listOutgoing ;
    String listStatus ;


    public Obj_ListExchange(int listId, int listIncoming, int listOutgoing, String listStatus) {
        this.listId = listId;
        this.listIncoming = listIncoming;
        this.listOutgoing = listOutgoing;
        this.listStatus = listStatus;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getListIncoming() {
        return listIncoming;
    }

    public void setListIncoming(int listIncoming) {
        this.listIncoming = listIncoming;
    }

    public int getListOutgoing() {
        return listOutgoing;
    }

    public void setListOutgoing(int listOutgoing) {
        this.listOutgoing = listOutgoing;
    }

    public String getListStatus() {
        return listStatus;
    }

    public void setListStatus(String listStatus) {
        this.listStatus = listStatus;
    }

    @Override
    public String toString() {
        return String.format(listId + " | " + listIncoming + " | "  + listOutgoing + " | " + listStatus);
    }
}
