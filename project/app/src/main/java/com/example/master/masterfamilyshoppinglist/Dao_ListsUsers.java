package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_ListsUsers {

    @Insert
    void insertListsUsers  (Obj_ListsUsers listsUsers);

    @Insert
    void insertMultipleListsUsers  (List<Obj_ListsUsers> listsUsersList);

    @Query("SELECT * FROM listsUsers WHERE listId = :listId ")
    Obj_ListsUsers fetchListsUsersById(int listId);

    @Query("SELECT * FROM listsUsers")
    List<Obj_ListsUsers> fetchAllListsUsers ();

    @Update
    void updateListsUsers (Obj_ListsUsers listsUsers);

    @Delete
    void deleteListsUsers (Obj_ListsUsers listsUsers);
}
