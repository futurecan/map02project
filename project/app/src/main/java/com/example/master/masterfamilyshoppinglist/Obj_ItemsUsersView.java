package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.text.SimpleDateFormat;

@Entity(tableName = "itemsUsers")


public class Obj_ItemsUsersView {

    @PrimaryKey(autoGenerate = true)
    int itemUserid ;
    int itemId ;
    String itemName ;
    String itemNote ;
    int itemCatId ;
    int listId ;
    String itemStatus ;
    String itemDoneDate ;
    Double itemPrice ;
    int itemStore ;
    int itemRequester ;
    int itemQty ;
    String requesteritemQtyName;

    public String getRequesteritemQtyName() {
        return requesteritemQtyName;
    }

    public void setRequesteritemQtyName(String requesteritemQtyName) {
        this.requesteritemQtyName = requesteritemQtyName;
    }

    public Obj_ItemsUsersView(int itemUserid, int itemId, String itemName, String itemNote, int itemCatId, int listId, String itemStatus, String itemDoneDate, Double itemPrice, int itemStore, int itemRequester, int itemQty, String requesteritemQtyName) {
        this.itemUserid = itemUserid;
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemNote = itemNote;
        this.itemCatId = itemCatId;
        this.listId = listId;
        this.itemStatus = itemStatus;
        this.itemDoneDate = itemDoneDate;
        this.itemPrice = itemPrice;
        this.itemStore = itemStore;
        this.itemRequester = itemRequester;

        this.itemQty = itemQty;
        this.requesteritemQtyName = requesteritemQtyName;
    }

    public int getItemUserid() {
        return itemUserid;
    }

    public void setItemUserid(int itemUserid) {
        this.itemUserid = itemUserid;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNote() {
        return itemNote;
    }

    public void setItemNote(String itemNote) {
        this.itemNote = itemNote;
    }

    public int getItemCatId() {
        return itemCatId;
    }

    public void setItemCatId(int itemCatId) {
        this.itemCatId = itemCatId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemDoneDate() {
        return itemDoneDate;
    }

    public void setItemDoneDate(String itemDoneDate) {
        this.itemDoneDate = itemDoneDate;
    }

    public Double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemStore() {
        return itemStore;
    }

    public void setItemStore(int itemStore) {
        this.itemStore = itemStore;
    }

    public int getItemRequester() {
        return itemRequester;
    }

    public void setItemRequester(int itemRequester) {
        this.itemRequester = itemRequester;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    @Override
    public String toString() {
        String doneDate = Z_Globals.dateFormatLocal.format(itemDoneDate);
        return String.format(itemUserid + " | " + itemId + " | " +  doneDate + " | " + itemName + " | " +  itemNote + " | " +
                itemCatId + " | " + listId + " | " + itemStatus + " | " + itemPrice + " | " + itemStore + " | " +  itemRequester + " | " + itemQty + " | " + requesteritemQtyName);
    }

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
}
