package com.example.master.masterfamilyshoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {
Button bt_login;
Button bt_register;
    EditText et_userName ;
    EditText et_password;
    EditText et_email;
    String token;
public static final String PREFS_NAME = "MyApp_Settings";
    public RetroService service ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // Go Back to Login ************
        bt_login = (Button)findViewById(R.id.bt_login);
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
                RegisterActivity.this.finish();

            }
        });
        //***************************
        et_userName=(EditText) findViewById(R.id.et_userName)  ;
        et_password =(EditText)findViewById(R.id.et_password) ;
        et_email=(EditText)findViewById(R.id.et_email) ;
        bt_register = (Button)findViewById(R.id.bt_register) ;
        bt_register.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                String email = et_email.getText().toString();
                token = (new  GenerateToken(email).token);

                Obj_Users user = new Obj_Users( 0,  et_userName.getText().toString(),  et_password.getText().toString(),  email,  token,  "2000-11-11",  "active");
                startRetrofit();
                Call<Obj_Users> calladduser = service.addUser(user);
                calladduser.enqueue(new Callback<Obj_Users>() {
                    @Override
                    public void onResponse(Call<Obj_Users> call, Response<Obj_Users> response) {
                        if (response.isSuccessful()) {

                            int id = response.body().userId;
                            token = response.body().token;
                            String userName = response.body().userName;
                            String userEmail = response.body().userEmail;
                            Toast.makeText(RegisterActivity.this, "Registered Successfuly" + id, Toast.LENGTH_LONG).show();
                            Z_Globals.TOKEN= token;
                            Z_Globals.MYID= id;
                            Z_Globals.DATABASE_NAME =  token;
                            SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("Token", token);
                            editor.putInt("ID", id);
                            editor.putString("USERNAME", userName);
                            editor.putString("USEREMAIL", userEmail);
                            editor.commit();

                            Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                            startActivity(intent);
                            RegisterActivity.this.finish();
                        } else {
                            try {
                                String error = response.errorBody().string();
                                Toast.makeText(RegisterActivity.this, "Invalid DATA  " + error , Toast.LENGTH_LONG).show();
                            } catch (IOException ex) {
                                Toast.makeText(RegisterActivity.this, "Register failed (3-IOEx)", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Obj_Users> call, Throwable t) {
                        Z_Globals.MYID=0;
                    }

                });





            }
        });
    }
    public void startRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();
        final String userEmail = et_userName.getText().toString();
        final String passsword = et_password.getText().toString();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();



                Request.Builder builder = originalRequest.newBuilder().header("Authorization",
                        Credentials.basic(userEmail, passsword));
//                Request.Builder builder = originalRequest.newBuilder();
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Z_Globals.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        service = retrofit.create(RetroService.class);
    }

}
