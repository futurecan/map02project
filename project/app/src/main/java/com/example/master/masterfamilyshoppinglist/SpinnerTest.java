package com.example.master.masterfamilyshoppinglist;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SpinnerTest extends AppCompatActivity implements
        AdapterView.OnItemSelectedListener {
    String[] categories;
    List<Obj_CategoryUsers  > categoryList;
    ArrayAdapter<Obj_CategoryUsers> categoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_test);
        //Getting the instance of Spinner and applying OnItemSelectedListener on it
        Spinner spin = (Spinner) findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);

        new LoadItemsFromDbAsyncTask().execute();
        //Creating the ArrayAdapter instance having the country list
        categoryList = new ArrayList<>();
        categoryAdapter = new ArrayAdapter<Obj_CategoryUsers>( SpinnerTest.this, android.R.layout.simple_list_item_1, categoryList);
        spin.setAdapter(categoryAdapter);

    }


    //Performing action onItemSelected and onNothing selected
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        //TODO
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadItemsFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_CategoryUsers>> {

        @Override
        protected List<Obj_CategoryUsers> doInBackground(Void... params) {
            List<Obj_CategoryUsers> list = Z_Globals.db.dao_categoryUsers().fetchAllCategoryUsers();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_CategoryUsers> list) {
            if (list == null) {
                Toast.makeText(SpinnerTest.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                categoryList.clear();
                for (Obj_CategoryUsers category : list) {
                    categoryList.add(category);
                }
                categoryAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//




}
