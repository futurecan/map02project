package com.example.master.masterfamilyshoppinglist;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
public interface RetroService {
    @GET("users")
    Call<List<Obj_Users>> getAllUsers();

    @POST("users/login")
    Call<Obj_Users> login(@Body Obj_Users user);

    @POST("users")
    Call<Obj_Users> addUser(@Body Obj_Users user);

    @POST("")
    Call<Obj_Circles> addCircle(@Body Obj_Circles circle) ;
}
