package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_ItemCategoriesDefault {

    @Insert
    void insertItemCategoriesDefault (Obj_ItemCategoriesDefault itemCategoriesDefault);

    @Insert
    void insertMultipleItemCategoriesDefault (List<Obj_ItemCategoriesDefault> itemCategoriesDefaultList);

    @Query("SELECT * FROM itemCategoriesDefault WHERE itemCatId = :itemCatId")
    Obj_ItemCategoriesDefault fetchItemCategoriesDefaultById  (int itemCatId);

    @Query("SELECT * FROM itemCategoriesDefault")
    List<Obj_ItemCategoriesDefault> fetchAllItemCategoriesDefault ();

    @Update
    void updateItemCategoriesDefault (Obj_ItemCategoriesDefault itemCategoriesDefault);

    @Delete
    void deleteItemCategoriesDefault (Obj_ItemCategoriesDefault itemCategoriesDefault);
}
