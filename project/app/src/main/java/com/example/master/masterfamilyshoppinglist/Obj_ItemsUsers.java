package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity(tableName = "itemsUsers")

public class Obj_ItemsUsers {

    @PrimaryKey(autoGenerate = true)
    int itemId ;
    int itemUserid ;
    String itemName ;
    String itemNote ;
    int itemCatId ;
    int listId ;
    String itemStatus ;
    String itemDoneDate ;
    String itemPrice ;
    int itemStore ;
    int itemRequester ;
    int itemQty ;

    public Obj_ItemsUsers(int itemUserid, int itemId, String itemName, String itemNote, int itemCatId, int listId, String itemStatus, String itemDoneDate, String itemPrice, int itemStore, int itemRequester, int itemQty) {
        this.itemUserid = itemUserid;
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemNote = itemNote;
        this.itemCatId = itemCatId;
        this.listId = listId;
        this.itemStatus = itemStatus;
        this.itemDoneDate = itemDoneDate;
        this.itemPrice = itemPrice;
        this.itemStore = itemStore;
        this.itemRequester = itemRequester;
        this.itemQty = itemQty;
    }

    public int getItemUserid() {
        return itemUserid;
    }

    public void setItemUserid(int itemUserid) {
        this.itemUserid = itemUserid;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNote() {
        return itemNote;
    }

    public void setItemNote(String itemNote) {
        this.itemNote = itemNote;
    }

    public int getItemCatId() {
        return itemCatId;
    }

    public void setItemCatId(int itemCatId) {
        this.itemCatId = itemCatId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemDoneDate() {
        return itemDoneDate;
    }

    public void setItemDoneDate(String itemDoneDate) {
        this.itemDoneDate = itemDoneDate;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemStore() {
        return itemStore;
    }

    public void setItemStore(int itemStore) {
        this.itemStore = itemStore;
    }

    public int getItemRequester() {
        return itemRequester;
    }

    public void setItemRequester(int itemRequester) {
        this.itemRequester = itemRequester;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    @Override
    public String toString() {
        return "Obj_ItemsUsers{" +
                ", itemId=" + itemId +
                ", itemName='" + itemName + '\'' +
                ", listId=" + listId +
                ", itemStatus='" + itemStatus + '\'' +
                ", itemStore=" + itemStore +
                ", itemQty=" + itemQty +
                '}';
    }

}
