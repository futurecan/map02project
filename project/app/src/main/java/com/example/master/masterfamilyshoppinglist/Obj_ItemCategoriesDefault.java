package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "itemCategoriesDefault")


public class Obj_ItemCategoriesDefault {

    @PrimaryKey(autoGenerate = true)
    int itemCatId ;
    String itemCatName;

    public int getCatId() {
        return itemCatId;
    }

    public void setCatId(int catId) {
        this.itemCatId = catId;
    }

    public String getCatName() {
        return itemCatName;
    }

    public void setCatName(String catName) {
        this.itemCatName = catName;
    }

    public Obj_ItemCategoriesDefault(int itemCatId, String itemCatName) {

        this.itemCatId = itemCatId;
        this.itemCatName = itemCatName;
    }

    @Override
    public String toString() {
        return String.format( itemCatId + " | " + itemCatName);
    }
}
