package com.example.master.masterfamilyshoppinglist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class F_CirclesViewList extends Fragment {


    ListView lv_circlesListView;
    List<Obj_Circles> circlesList;
    ArrayAdapter<Obj_Circles> circlesAdapter;
    public Obj_Circles selectedItem;

    FloatingActionButton fabCircles;

    public F_CirclesViewList() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate( R.layout.fragment_circle_list_view, container, false );

        lv_circlesListView = (ListView) view.findViewById( R.id.lv_circlesListView );
        circlesList = new ArrayList<>();
        circlesAdapter = new circlesArrayAdapter( getContext(), circlesList );

        lv_circlesListView.setAdapter( circlesAdapter );

        lv_circlesListView.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // String nameClicked = (String) parent.getItemAtPosition(position);
                Obj_Circles todoClicked = (Obj_Circles) parent.getItemAtPosition( position );
                showDeleteDialogBox( todoClicked );
                ;
                return true;
            }
        } );

        //--------------------------------------------------------------------------------------------//
        /////////////*************  Floating Button to add new Record   ***************////////////
        //--------------------------------------------------------------------------------------------//

        fabCircles = (FloatingActionButton) view.findViewById( R.id.fabCirclesList );
        fabCircles.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent( getActivity(), AddCircleActivity.class );
                startActivity( intent );
            }
        } );

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        fabCircles.hide();
    }

    @Override
    public void onStart() {
        super.onStart();
        fabCircles.show();
        new LoadCircleFromDbAsyncTask().execute();
    }

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadCircleFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_Circles>> {

        @Override
        protected List<Obj_Circles> doInBackground(Void... params) {
            List<Obj_Circles> list = Z_Globals.db.dao_circles().fetchAllCircles();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_Circles> list) {
            if (list == null) {
                Toast.makeText( getActivity(), "Database error fetching data", Toast.LENGTH_LONG ).show();
            } else {
                // WRONG: friendsList = list;
                circlesList.clear();
                for (Obj_Circles circle : list) {
                    circlesList.add( circle );
                }
                circlesAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//


    //--------------------------------------------------------------------------------------------//
    /////////////*************  Remove from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class RemoveCircleDbAsyncTask extends AsyncTask<Obj_Circles, Void, Void> {

        @Override
        protected Void doInBackground(Obj_Circles... params) {
            Obj_Circles circle = params[0];
            Z_Globals.db.dao_circles().deleteCircles( circle );
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            new LoadCircleFromDbAsyncTask().execute(); // reload the list
            Toast.makeText( F_CirclesViewList.this.getActivity(), "Circle Deleted ", Toast.LENGTH_LONG ).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Show Dialog Box to Confirm Removing   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void showDeleteDialogBox(final Obj_Circles circle) {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
        builder.setTitle( "Confirm Deleting Store " );
        // setup the additional edit text
        builder.setPositiveButton( "Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new RemoveCircleDbAsyncTask().execute( circle );
            }

        } );
        builder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        } );
        builder.show();

    }

    public class circlesArrayAdapter extends ArrayAdapter<Obj_Circles> {
        private Context context;
        private List<Obj_Circles> list;

        public circlesArrayAdapter(Context context, List<Obj_Circles> list) {
            super( context, R.layout.fragment_circle_custom_view, list );
            this.context = context;
            this.list = list;
        }

        @Override ////////******** Build List View    ********//////////////
        public View getView(int position, final View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            // FIXME: reuse convert view if available
            View rowView = (View) inflater.inflate( R.layout.fragment_circle_custom_view, parent, false );

            final TextView tv_circleName = (TextView) rowView.findViewById( R.id.tv_circleName );
            final TextView tv_circleDescription = (TextView) rowView.findViewById( R.id.tv_circleDescription );
            ImageView iv_circleView = (ImageView) rowView.findViewById( R.id.iv_circleView );

            //
            selectedItem = list.get( position );
            final Obj_Circles itemButtonClicked = (Obj_Circles) selectedItem;
            tv_circleName.setText( selectedItem.circleName );
            tv_circleDescription.setText( selectedItem.circleDescription );
            final int circleId = selectedItem.circleId;

            iv_circleView.setFocusable( true );
            iv_circleView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    //String myListId = Integer.toString(selectedItem.listId);

                    bundle.putString( "circle name", tv_circleName.getText().toString() );
                    bundle.putString( "circle description", tv_circleDescription.getText().toString() );
                    bundle.putInt( "circleId", circleId );

                    F_CircleDetails fragment = new F_CircleDetails();
                    fragment.setArguments( bundle );
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace( R.id.fragment_container, fragment );
                    fragmentTransaction.commit();
                    getActivity().setTitle( selectedItem.circleName );


                }

            } );
            //
            return rowView;
        }
    }


}
