package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "users")

public class Obj_Users {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("userId")
    int userId ;
    @SerializedName("userName")
    String userName;
    @SerializedName("password")
    String password ;
    @SerializedName("userEmail")
    String userEmail ;
    @SerializedName("token")
    String token ;
    @SerializedName("userLastLogin")
    String userLastLogin ;
    @SerializedName("userStatus")
    String userStatus ;

    public Obj_Users() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return password;
    }

    public void setUserPassword(String userPassword) {
        this.password = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserToken() {
        return token;
    }

    public void setUserToken(String userToken) {
        this.token = userToken;
    }

    public String getUserLastLogin() {
        return userLastLogin;
    }

    public void setUserLastLogin(String userLastLogin) {
        this.userLastLogin = userLastLogin;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Obj_Users(int userId, String userName, String userPassword, String userEmail, String userToken, String userLastLogin, String userStatus) {
        this.userId = userId;
        this.userName = userName;
        this.password = userPassword;
        this.userEmail = userEmail;
        this.token = userToken;
        this.userLastLogin = userLastLogin;
        this.userStatus = userStatus;
    }

    @Override
    public String toString() {
        return "Obj_Users{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", token='" + token + '\'' +
                ", userLastLogin='" + userLastLogin + '\'' +
                ", userStatus='" + userStatus + '\'' +
                '}';
    }
//    @Override
//    public String toString() {
//        String loginDate = Z_Globals.dateFormatLocal.format(userLastLogin);
//        return String.format( userId + " | " +  userName + " | " + password + " | " + userEmail + " | " +  userToken + " | " +  userLastLogin + " | " + userStatus);
//    }
}
