package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_Friends {

    @Insert
    void insertFriend(Obj_Friends friends);

    @Insert
    void insertMultipleFriends(List<Obj_Friends> friendsList);

    @Query("SELECT * FROM friends WHERE connectId = :friendId")
    Obj_Friends fetchFriendById(int friendId);

    @Query("SELECT * FROM Friends")
    List<Obj_Friends> fetchAllFriends();

    @Update
    void updateFriend(Obj_Friends friends);

    @Delete
    void deleteFriend(Obj_Friends friends);
}
