package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "itemsDefault")


public class Obj_ItemsDefault {

    @PrimaryKey(autoGenerate = true)
    int itemId ;
    String itemName ;
    String itemDescription ;
    int itemCat ;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getItemCat() {
        return itemCat;
    }

    public void setItemCat(int itemCat) {
        this.itemCat = itemCat;
    }

    public Obj_ItemsDefault(int itemId, String itemName, String itemDescription, int itemCat) {

        this.itemId = itemId;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemCat = itemCat;
    }

    @Override
    public String toString() {
        return String.format(itemId + " | " + itemName + " | " + itemDescription + " | " +  itemCat);
    }
}
