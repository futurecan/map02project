package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCategoryActivity extends AppCompatActivity {

    EditText et_catName;
    Button bt_addCat;

    private android.support.v7.widget.Toolbar toolbar;

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
        //ToolBar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        et_catName = (EditText) findViewById(R.id.et_catName);
        bt_addCat = (Button) findViewById(R.id.bt_addCat);


        bt_addCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String category = et_catName.getText().toString();
                new InsertCatDbAsyncTask().execute( new Obj_CategoryUsers(0, category , 0));

                Toast.makeText(AddCategoryActivity.this, "Store is added ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    class InsertCatDbAsyncTask extends AsyncTask<Obj_CategoryUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_CategoryUsers... params) {
            Obj_CategoryUsers category = params[0];
            Z_Globals.db.dao_categoryUsers().insertCategoryUsers(category);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            // Toast
            finish();
        }

    }
}

