package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "circleUsers")


public class Obj_CircleUsers {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("circleUsersId")
    int circleUsersId ;
    @SerializedName("circleUserStatus")
   String circleUserStatus  ;
   // enum CircleUserStatus {pending , accepted , rejected}

    public Obj_CircleUsers(int circleUsersId, String circleUserStatus ) {
        this.circleUsersId = circleUsersId;
        this.circleUserStatus = circleUserStatus;
    }


    public int getCircleUsersId() {
        return circleUsersId;
    }

    public void setCircleUsersId(int circleUsersId) {
        this.circleUsersId = circleUsersId;
    }

    public String getCircleUserStatus() {
        return circleUserStatus;
    }

    public void setCircleUserStatus(String circleUserStatus) {
        this.circleUserStatus = circleUserStatus;
    }

    @Override
    public String toString() {
        return String.format(circleUsersId+" | "+ circleUserStatus);
    }
}
