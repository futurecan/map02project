package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_Stores {


    @Insert
    void insertStores (Obj_Stores stores);

    @Insert
    void insertMultipleStores (List<Obj_Stores> storesList);

    @Query("SELECT * FROM stores WHERE storeId = :storeId")
    Obj_Stores fetchStoresById(int storeId);

    @Query("SELECT * FROM stores")
    List<Obj_Stores> fetchAllStores();

    @Update
    void updateStores(Obj_Stores stores);

    @Delete
    void deleteStores (Obj_Stores stores);
}
