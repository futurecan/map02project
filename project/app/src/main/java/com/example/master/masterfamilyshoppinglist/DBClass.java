package com.example.master.masterfamilyshoppinglist;


import java.util.ArrayList;
import java.util.Date;

public class DBClass {
public ArrayList<Obj_Users> db_users ;
public ArrayList<Obj_CategoryUsers> db_CategoryUsers ;
public ArrayList<Obj_Circles> db_Circles ;
public ArrayList<Obj_Friends> db_Friends ;
public ArrayList<Obj_ListExchange> db_ListExchange ;
public ArrayList<Obj_ItemsUsers> db_ItemsUserst ;
public ArrayList<Obj_Notifications> db_Notifications ;
public ArrayList<Obj_ListsUsers> db_ListsUser ;
public ArrayList<Obj_CircleUsers> db_CircleUsers ;
public ArrayList<Obj_Stores> db_Stores ;
public ArrayList<Obj_ItemCategoriesDefault> db_ItemCategoriesDefault ;
public ArrayList<Obj_ItemsDefault> db_ItemsDefault ;

    public DBClass() {
    }

    public ArrayList<Obj_Users> getDb_users() {
        return db_users;
    }

    public ArrayList<Obj_CategoryUsers> getDb_CategoryUsers() {
        return db_CategoryUsers;
    }

    public ArrayList<Obj_Circles> getDb_Circles() {
        return db_Circles;
    }

    public ArrayList<Obj_Friends> getDb_Friends() {
        return db_Friends;
    }

    public ArrayList<Obj_ListExchange> getDb_ListExchange() {
        return db_ListExchange;
    }

    public ArrayList<Obj_ItemsUsers> getDb_ItemsUserst() {
        return db_ItemsUserst;
    }

    public ArrayList<Obj_Notifications> getDb_Notifications() {
        return db_Notifications;
    }

    public ArrayList<Obj_ListsUsers> getDb_ListsUser() {
        return db_ListsUser;
    }

    public ArrayList<Obj_CircleUsers> getDb_CircleUsers() {
        return db_CircleUsers;
    }

    public ArrayList<Obj_Stores> getDb_Stores() {
        return db_Stores;
    }

    public ArrayList<Obj_ItemCategoriesDefault> getDb_ItemCategoriesDefault() {
        return db_ItemCategoriesDefault;
    }

    public ArrayList<Obj_ItemsDefault> getDb_ItemsDefault() {
        return db_ItemsDefault;
    }

    public void setDb_users(ArrayList<Obj_Users> db_users) {
        db_users = new ArrayList<Obj_Users> ();
        Date userLastLogin =new Date() ;
       // Obj_Users newUser = new Obj_Users (1,"Remon","r.setup@yahoo.com","KHG654GDSbhoi",userLastLogin,"active");
      //  db_users.add(newUser);
        this.db_users = db_users;
    }

    public void setDb_CategoryUsers(ArrayList<Obj_CategoryUsers> db_CategoryUsers) {

        Obj_CategoryUsers CategoryUsers = new Obj_CategoryUsers( 1,  "Dairy",  1);
        db_CategoryUsers.add(CategoryUsers);
        CategoryUsers = new Obj_CategoryUsers( 1,  "Dairy",  1);
        db_CategoryUsers.add(CategoryUsers);
        CategoryUsers = new Obj_CategoryUsers( 2,  "Cold Cut",  1);
        db_CategoryUsers.add(CategoryUsers);
        CategoryUsers = new Obj_CategoryUsers( 3,  "Bakery",  1);
        db_CategoryUsers.add(CategoryUsers);
        CategoryUsers = new Obj_CategoryUsers( 4,  "Grocery",  1);
        db_CategoryUsers.add(CategoryUsers);

        this.db_CategoryUsers = db_CategoryUsers;
    }

    public void setDb_Circles(ArrayList<Obj_Circles> db_Circles) {
        Obj_Circles  circles = new Obj_Circles( 1,  "Home",  1,  "Home Circle", "pending");
        db_Circles.add(circles) ;
        circles = new Obj_Circles( 2,  "Work",  1,  "Work Circle", "accepted");
        db_Circles.add(circles) ;
        circles = new Obj_Circles( 3,  "Best buddies",  1,  "Old School friends circle", "rejected");
        db_Circles.add(circles) ;
        circles = new Obj_Circles( 4,  "Mom",  1,  "Mom's Circle", "accepted");
        db_Circles.add(circles) ;

        this.db_Circles = db_Circles;
    }

    public void setDb_Friends(ArrayList<Obj_Friends> db_Friends) {
        Obj_Friends friends = new Obj_Friends(1,2,1) ;
        db_Friends.add(friends) ;
        friends = new Obj_Friends(2 , 1 , 3) ;
        db_Friends.add(friends) ;
        friends = new Obj_Friends(3 , 3 , 2) ;
        db_Friends.add(friends) ;

        this.db_Friends = db_Friends;
    }

    public void setDb_ListExchange(ArrayList<Obj_ListExchange> db_ListExchange) {
        Obj_ListExchange list = new Obj_ListExchange(1 , 1 , 3 , "pending") ;
        db_ListExchange.add(list) ;
        list = new Obj_ListExchange(2,4,2 , "accepted") ;
        db_ListExchange.add(list) ;
        list = new Obj_ListExchange(3 , 7,4, "rejected") ;
        db_ListExchange.add(list);

        this.db_ListExchange = db_ListExchange;
    }

//    public void setDb_ItemsUserst(ArrayList<Obj_ItemsUsers> db_ItemsUserst) {
//        Date myDate = new Date () ;
//        Obj_ItemsUsers itemsUsers = new Obj_ItemsUsers(2, 1 , "Milk" , "Skimmed" , 1 , 3, "pending" , "sept 08,2018 " , 4.50 , 1 , 2 ,4 );
//        db_ItemsUserst.add(itemsUsers) ;
//        itemsUsers = new Obj_ItemsUsers(3, 2 , "chocolate" , "Snickers" , 4 , 1, "done" , "Jan 15,2019" , 3.00 , 2 , 1 ,3 );
//        db_ItemsUserst.add(itemsUsers) ;
//        itemsUsers = new Obj_ItemsUsers(1, 1 , "Bread" , "Full Grain" , 3 , 2, "rejected" , "Feb 10,2020" , 1.50 , 1 , 2 ,4 );
//        db_ItemsUserst.add(itemsUsers) ;
//
//        this.db_ItemsUserst = db_ItemsUserst;
//    }

    public void setDb_Notifications(ArrayList<Obj_Notifications> db_Notifications) {
        Obj_Notifications notification = new Obj_Notifications( 1 , 2 , 1 ,"item", "pending" , 1 ) ;
        db_Notifications.add (notification) ;
        notification = new Obj_Notifications( 2 , 4 , 2 , "List", "done" , 3 ) ;
        db_Notifications.add (notification) ;
        notification = new Obj_Notifications( 3 , 3 , 1 , "circle", "pending" , 2 ) ;
        db_Notifications.add (notification) ;

        this.db_Notifications = db_Notifications;
    }

    public void setDb_ListsUser(ArrayList<Obj_ListsUsers> db_ListsUser) {
        Obj_ListsUsers listsUsers = new Obj_ListsUsers(1, 2 , "weekly grocery" , "" , "active", "saved") ;
        db_ListsUser.add(listsUsers) ;
        listsUsers = new Obj_ListsUsers(2 , 2 , "Christmas list" , "" , "inactive", "saved") ;
        db_ListsUser.add(listsUsers) ;
        listsUsers = new Obj_ListsUsers(3, 2 , "Renovation List" , "" , "active", "unsaved") ;
        db_ListsUser.add(listsUsers) ;

        this.db_ListsUser = db_ListsUser;
    }

    public void setDb_CircleUsers(ArrayList<Obj_CircleUsers> db_CircleUsers) {
        Obj_CircleUsers circleUsers = new Obj_CircleUsers(1 , "pending");
        db_CircleUsers.add(circleUsers) ;
        circleUsers = new Obj_CircleUsers(2 , "accepted");
        db_CircleUsers.add(circleUsers) ;
        circleUsers = new Obj_CircleUsers(3 , "rejected");
        db_CircleUsers.add(circleUsers) ;

        this.db_CircleUsers = db_CircleUsers;
    }

    public void setDb_Stores(ArrayList<Obj_Stores> db_Stores) {
        Obj_Stores stores = new Obj_Stores(1, "Walmart") ;
        db_Stores.add(stores) ;
        stores = new Obj_Stores(2 , "Costco");
        db_Stores.add(stores) ;
        stores = new Obj_Stores(3 , "Maxi");
        db_Stores.add(stores) ;
        stores = new Obj_Stores(4 , "Andalous");
        db_Stores.add(stores) ;

        this.db_Stores = db_Stores;
    }

    public void setDb_ItemCategoriesDefault(ArrayList<Obj_ItemCategoriesDefault> db_ItemCategoriesDefault) {
        Obj_ItemCategoriesDefault itemCategoriesDefault = new Obj_ItemCategoriesDefault(1, "Bakery") ;
        db_ItemCategoriesDefault.add(itemCategoriesDefault) ;
        itemCategoriesDefault = new Obj_ItemCategoriesDefault(2, "Grocery") ;
        db_ItemCategoriesDefault.add(itemCategoriesDefault) ;
        itemCategoriesDefault = new Obj_ItemCategoriesDefault(3, "Dairy") ;
        db_ItemCategoriesDefault.add(itemCategoriesDefault) ;
        itemCategoriesDefault = new Obj_ItemCategoriesDefault(4, "Tools") ;
        db_ItemCategoriesDefault.add(itemCategoriesDefault) ;
        this.db_ItemCategoriesDefault = db_ItemCategoriesDefault;
    }

    public void setDb_ItemsDefault(ArrayList<Obj_ItemsDefault> db_ItemsDefault) {
        Obj_ItemsDefault itemsDefault = new Obj_ItemsDefault(1 , "fava beans" , "" , 2 ) ;
        db_ItemsDefault.add(itemsDefault) ;
        itemsDefault = new Obj_ItemsDefault(2 , "Coffee" , "" , 1 ) ;
        db_ItemsDefault.add(itemsDefault) ;
        this.db_ItemsDefault = db_ItemsDefault;
    }
}
