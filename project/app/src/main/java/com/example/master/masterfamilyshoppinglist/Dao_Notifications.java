package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_Notifications {

    @Insert
    void insertNotifications (Obj_Notifications notifications);

    @Insert
    void insertMultipleNotifications (List<Obj_Notifications> notificationsList);

    @Query("SELECT * FROM notifications WHERE notifId = :notifId")
    Obj_Notifications fetchNotificationsById(int notifId);

    @Query("SELECT * FROM notifications")
    List<Obj_Notifications> fetchAllNotifications ();

    @Update
    void updateNotifications(Obj_Notifications notifications);

    @Delete
    void deleteNotifications (Obj_Notifications notifications);
}
