package com.example.master.masterfamilyshoppinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_ListExchange {

    @Insert
    void insertListExchange  (Obj_ListExchange listExchange);

    @Insert
    void insertMultipleListExchange  (List<Obj_ListExchange> listExchangeList);

    @Query("SELECT * FROM listExchange WHERE listId = :listId ")
    Obj_ListExchange fetchListExchangeById(int listId);

    @Query("SELECT * FROM listExchange")
    List<Obj_ListExchange> fetchAllListExchange ();

    @Update
    void updateListExchange (Obj_ListExchange listExchange);

    @Delete
    void deleteListExchange (Obj_ListExchange listExchange);
}
