package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

public class AddShoppingList extends AppCompatActivity {

    public static final String EXTRA_INDEX = "index";

    EditText et_addListName ;
    EditText et_listNotes ;
    Switch sw_saveToFav ;
    Button bt_addList ;


    private android.support.v7.widget.Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shopping_list);


        et_addListName = (EditText) findViewById(R.id.et_addListName) ;
        et_listNotes = (EditText) findViewById(R.id.et_listNotes) ;
        sw_saveToFav = (Switch) findViewById(R.id.sw_saveToFav) ;
        bt_addList = (Button) findViewById(R.id.bt_addShoppingList) ;

        bt_addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = et_addListName.getText().toString() ;
                String notes = et_listNotes.getText().toString();
                new InsertShoppingDbAsyncTask().execute(new Obj_ListsUsers(0 , 0 , name , notes , "active" , "Saved")) ;

                Toast.makeText(AddShoppingList.this, "List is added " , Toast.LENGTH_SHORT).show();

//                Intent intent = new Intent(AddShoppingList.this, F_ItemListView.class);
//               intent.putExtra(EXTRA_INDEX, name);
//                startActivity(intent);
                finish();
            }
        });

    }

    class InsertShoppingDbAsyncTask extends AsyncTask<Obj_ListsUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_ListsUsers... params) {
            Obj_ListsUsers list = params[0];
            Z_Globals.db.dao_listsUsers().insertListsUsers(list);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            // Toast
            finish();
        }

    }

}
