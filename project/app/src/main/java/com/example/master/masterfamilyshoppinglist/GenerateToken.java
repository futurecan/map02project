package com.example.master.masterfamilyshoppinglist;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GenerateToken {
    String email;
    String token;


    public  GenerateToken(String email) {

        this.email = email;
        token =getToken( email);

    }
    private String getToken(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

            return hexString.toString();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
