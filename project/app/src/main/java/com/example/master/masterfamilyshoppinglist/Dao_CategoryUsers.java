package com.example.master.masterfamilyshoppinglist;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface Dao_CategoryUsers {

    @Insert
    void insertCategoryUsers (Obj_CategoryUsers categoryUsers);

    @Insert
    void insertMultipleCategoryUsers (List<Obj_CategoryUsers> categoryUsersList);

    @Query("SELECT * FROM categoriesUsers WHERE catId = :categoryId")
    Obj_CategoryUsers fetchCategoryUsersById(int categoryId);

    @Query("SELECT * FROM categoriesUsers")
    List<Obj_CategoryUsers> fetchAllCategoryUsers();

    @Update
    void updateCategoryUsers (Obj_CategoryUsers categoryUsers);

    @Delete
    void deleteCategoryUsers (Obj_CategoryUsers categoryUsers);
}
