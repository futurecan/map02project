package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class F_ShoppingListsView extends Fragment {

    public static final String EXTRA_INDEX = "1";
    public static final String EXTRA_TEXT = "listName";
    private AppDatabase appDatabase = Z_Globals.db;
    ListView lvShoppingList;
    List<Obj_ListsUsers> shoppingListList;
    ArrayAdapter<Obj_ListsUsers> shoppingAdapter;
    public Obj_ListsUsers selectedItem;

    private android.support.v7.widget.Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    FloatingActionButton fabShopping;

    public F_ShoppingListsView() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shoppinglist_list_view, container, false);
        lvShoppingList = (ListView) view.findViewById(R.id.lv_shoppingList);

        shoppingListList = new ArrayList<>();
        shoppingAdapter = new shoppingAdapter(getContext(),  shoppingListList);

        lvShoppingList.setAdapter(shoppingAdapter);
        lvShoppingList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // String nameClicked = (String) parent.getItemAtPosition(position);
                Obj_ListsUsers todoClicked = (Obj_ListsUsers) parent.getItemAtPosition(position);
                showDeleteDialogBox(todoClicked);
                ;
                return true;
            }
        });



        //--------------------------------------------------------------------------------------------//
        /////////////*************  Floating Button to add new Record   ***************////////////
        //--------------------------------------------------------------------------------------------//
        fabShopping = (FloatingActionButton) view.findViewById(R.id.fabShoppingList);
        fabShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddShoppingList.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        fabShopping.hide();
    }

    @Override
    public void onStart() {
        super.onStart();
        fabShopping.show();
        new LoadListsFromDbAsyncTask().execute();
    }

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadListsFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_ListsUsers>> {

        @Override
        protected List<Obj_ListsUsers> doInBackground(Void... params) {
            List<Obj_ListsUsers> list = Z_Globals.db.dao_listsUsers().fetchAllListsUsers();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_ListsUsers> list) {
            if (list == null) {
                Toast.makeText(getActivity(), "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                shoppingListList.clear();
                for (Obj_ListsUsers myLists : list) {
                    shoppingListList.add(myLists);
                }
                shoppingAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//


    //--------------------------------------------------------------------------------------------//
    /////////////*************  Remove from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class RemoveListDbAsyncTask extends AsyncTask<Obj_ListsUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_ListsUsers... params) {
            Obj_ListsUsers list = params[0];
            Z_Globals.db.dao_listsUsers().deleteListsUsers(list);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            new LoadListsFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(F_ShoppingListsView.this.getActivity(), "Shopping List Deleted ", Toast.LENGTH_LONG).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Show Dialog Box to Confirm Removing   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void showDeleteDialogBox(final Obj_ListsUsers list) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm Deleting Shopping List ");
        // setup the additional edit text
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new RemoveListDbAsyncTask().execute(list);
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();


    }

    public class shoppingAdapter  extends ArrayAdapter<Obj_ListsUsers> {
        private Context context;
        private List<Obj_ListsUsers> list;

        public shoppingAdapter(Context context, List<Obj_ListsUsers> list) {
            super(context, R.layout.fragment_shopping_custom_list, list);
            this.context = context;
            this.list = list;
        }

        @Override ////////******** Build List View    ********//////////////
        public View getView(int position, final View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = (View) inflater.inflate(R.layout.fragment_shopping_custom_list, parent, false);

            final TextView tvListName = (TextView) rowView.findViewById(R.id.tv_listName);
            ImageView iv_listShare = (ImageView) rowView.findViewById(R.id.iv_listShare) ;
            ImageView iv_listView = (ImageView) rowView.findViewById(R.id.iv_listView) ;

            //
            selectedItem = list.get(position);
            final Obj_ListsUsers itemButtonClicked=(Obj_ListsUsers) selectedItem;
            tvListName.setText(selectedItem.listName);
            final int listId = selectedItem.listId;

            iv_listView.setFocusable(true);
            iv_listView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {

                    Bundle bundle = new Bundle();
                    //String myListId = Integer.toString(selectedItem.listId);

                    bundle.putString( "list" ,tvListName.getText().toString() );
                    bundle.putInt("listId",listId);

                    F_ItemListView fragment = new F_ItemListView();
                    fragment.setArguments(bundle);
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction() ;
                    fragmentTransaction.replace(R.id.fragment_container, fragment) ;
                    fragmentTransaction.commit();
                    getActivity().setTitle(selectedItem.listName );


                }

            });
            //
            return rowView;
        }
    }
}
