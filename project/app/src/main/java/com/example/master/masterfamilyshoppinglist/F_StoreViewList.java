package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class F_StoreViewList extends Fragment {

    ListView lvStores;
    List<Obj_Stores> storeList;
    ArrayAdapter<Obj_Stores> storesAdapter;

    private android.support.v7.widget.Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static final String DATABASE_NAME = "fsldb";
    FloatingActionButton fabStore;

    public F_StoreViewList() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_view_list, container, false);

        lvStores = (ListView) view.findViewById(R.id.lv_storeListView);
        storeList = new ArrayList<>();
        storesAdapter = new ArrayAdapter<Obj_Stores>(getActivity(), android.R.layout.simple_list_item_1, storeList);
        lvStores.setAdapter(storesAdapter);

        lvStores.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
              // String nameClicked = (String) parent.getItemAtPosition(position);
                Obj_Stores todoClicked = (Obj_Stores) parent.getItemAtPosition(position);
               showDeleteDialogBox (todoClicked); ;
               return true;
            }
        });

        //--------------------------------------------------------------------------------------------//
        /////////////*************  Floating Button to add new Record   ***************////////////
        //--------------------------------------------------------------------------------------------//
        fabStore = (FloatingActionButton) view.findViewById(R.id.fabStore);
        fabStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity() , AddStoreActivity.class);
                startActivity(intent);
            }
        });
        //--------------------------------------------------------------------------------------------//

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        fabStore.hide();
    }

    @Override
    public void onStart() {
        super.onStart();
        fabStore.show();
        new LoadStoreFromDbAsyncTask().execute() ;
    }
    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadStoreFromDbAsyncTask extends AsyncTask<Void, Void, List<Obj_Stores>> {

        @Override
        protected List<Obj_Stores> doInBackground(Void... params) {
            List<Obj_Stores> list = Z_Globals.db.dao_stores().fetchAllStores();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_Stores> list) {
            if (list == null) {
                Toast.makeText(getActivity(), "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                storeList.clear();
                for (Obj_Stores store : list) {
                    storeList.add(store);
                }
                storesAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//


    //--------------------------------------------------------------------------------------------//
    /////////////*************  Remove from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class RemoveStoreDbAsyncTask extends AsyncTask<Obj_Stores, Void, Void> {

        @Override
        protected Void doInBackground(Obj_Stores... params) {
            Obj_Stores store = params[0];
            Z_Globals.db.dao_stores().deleteStores(store);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }
        @Override
        protected void onPostExecute(Void list) {
            new LoadStoreFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(F_StoreViewList.this.getActivity(), "Store Deleted ", Toast.LENGTH_LONG).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Show Dialog Box to Confirm Removing   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void showDeleteDialogBox (final Obj_Stores store) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm Deleting Store " );
        // setup the additional edit text
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new RemoveStoreDbAsyncTask ().execute(store) ;
                }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

}

}
