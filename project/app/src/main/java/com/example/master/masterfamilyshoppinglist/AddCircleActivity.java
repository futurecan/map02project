package com.example.master.masterfamilyshoppinglist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddCircleActivity extends AppCompatActivity {
    public static final String BASE_API_URL2 = "http://www.familyshoppinglist.canadaresources.ca/api/api.php/circles/"+ Z_Globals.TOKEN;
    public RetroService service ;
    public int returncircleId;
    EditText et_CircleName ;
    EditText et_CicleDescription ;
    Button bt_AddCircle ;

    private android.support.v7.widget.Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_circle);
        et_CircleName = (EditText) findViewById(R.id.et_circleName);
        et_CicleDescription = (EditText) findViewById(R.id.et_circleDescription) ;
        bt_AddCircle = (Button) findViewById(R.id.bt_addCircle) ;


        bt_AddCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = et_CircleName.getText().toString() ;
                String description = et_CicleDescription.getText().toString();
                Obj_Circles circle = new Obj_Circles (0 , name , Z_Globals.MYID, description, "Active");
                recordCentral(circle);
                new InsertCircleDbAsyncTask().execute( new Obj_Circles (returncircleId , name , Z_Globals.MYID, description, "Active")) ;

                Toast.makeText(AddCircleActivity.this, "Circle is added " , Toast.LENGTH_SHORT).show();
            }
        });


    }

    public  void recordCentral (Obj_Circles circle){
        startRetrofit();
        Call<Obj_Circles> calladdCircle = service.addCircle(circle);
        returncircleId = 0;
        calladdCircle.enqueue(new Callback<Obj_Circles>() {
            @Override
            public void onResponse(Call<Obj_Circles> call, Response<Obj_Circles> response) {

                if (response.isSuccessful()) {

                    returncircleId = response.body().circleId;


                } else {
                    try {
                        String error = response.errorBody().string();
                        Toast.makeText(AddCircleActivity.this, "Invalid DATA  " + error , Toast.LENGTH_LONG).show();
                    } catch (IOException ex) {
                        Toast.makeText(AddCircleActivity.this, "New Circle failed (3-IOEx)", Toast.LENGTH_LONG).show();
                    }

                }
            }


            @Override
            public void onFailure(Call<Obj_Circles> call, Throwable t) {

            }

        });
}


    class InsertCircleDbAsyncTask extends AsyncTask<Obj_Circles, Void, Void> {

        @Override
        protected Void doInBackground(Obj_Circles... params) {
            Obj_Circles circle = params[0];
            Z_Globals.db.dao_circles().insertCircles (circle);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            // Toast
            finish();
        }

    }

    public void startRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();




               Request.Builder builder = originalRequest.newBuilder();
                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl(BASE_API_URL2)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        service = retrofit.create(RetroService.class);
    }

}
