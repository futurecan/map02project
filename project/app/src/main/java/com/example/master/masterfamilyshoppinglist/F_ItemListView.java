package com.example.master.masterfamilyshoppinglist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class F_ItemListView extends Fragment {

    FloatingActionButton fabItemList ;
int list_id ;
   // public final int listId ;

    ListView lv_itemList ;
    List<Obj_ItemsUsers> itemsList ;
    ArrayAdapter<Obj_ItemsUsers> itemAdapter ;
    public Obj_ItemsUsers selectedItem;

    public F_ItemListView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_list_view, container, false);
        lv_itemList = (ListView) view.findViewById(R.id.lv_itemList);

        itemsList = new ArrayList<>();
        //shoppingAdapter = new shoppingAdapter(getContext(),  shoppingListList);
        itemAdapter = new itemArrayAdapter(getContext(), android.R.layout.simple_list_item_1, itemsList);

        lv_itemList.setAdapter(itemAdapter);
        lv_itemList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // String nameClicked = (String) parent.getItemAtPosition(position);
                Obj_ItemsUsers onClick = (Obj_ItemsUsers) parent.getItemAtPosition(position);
                showDeleteDialogBox (onClick);
                return true;
            }
        });


        //--------------------------------------------------------------------------------------------//
        /////////////*************  Floating Button to add new Record   ***************////////////
        //--------------------------------------------------------------------------------------------//
        fabItemList = (FloatingActionButton) view.findViewById(R.id.fabItemList);
        fabItemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddItemActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        fabItemList.hide();
    }

    @Override
    public void onStart() {
        super.onStart();
        fabItemList.show();
        final int listId = this.getArguments().getInt("listId");
        list_id = listId;
        new LoadItemsFromDbAsyncTask().execute(listId);
    }
    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadItemsFromDbAsyncTask extends AsyncTask<Integer, Void  , List<Obj_ItemsUsers> >{


        @Override
        protected List<Obj_ItemsUsers> doInBackground(Integer... params) {
            int listId = params [0];
            List<Obj_ItemsUsers> list = (List<Obj_ItemsUsers>) Z_Globals.db.dao_itemsUsers().fetchItemsItemsUsersByList(listId);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Obj_ItemsUsers> list) {
            if (list == null) {
                Toast.makeText(getActivity(), "No items to show , please create items", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                itemsList.clear();
                for (Obj_ItemsUsers myLists : list) {
                    itemsList.add(myLists);
                }
                itemAdapter.notifyDataSetChanged();
            }
        }

    }
    //--------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------//
    /////////////*************  Mark Item Done By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class MarkdDoneDbAsyncTask extends AsyncTask<Obj_ItemsUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_ItemsUsers... params) {
            Obj_ItemsUsers item = params[0];
            Z_Globals.db.dao_itemsUsers().markItemDone(item.itemId);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {

            Toast.makeText(F_ItemListView.this.getActivity(), "Item maked done ", Toast.LENGTH_LONG).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Remove from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class RemoveListDbAsyncTask extends AsyncTask<Obj_ItemsUsers, Void, Void> {

        @Override
        protected Void doInBackground(Obj_ItemsUsers... params) {
            Obj_ItemsUsers item = params[0];
            Z_Globals.db.dao_itemsUsers().markItemDone(item.itemId);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            new LoadItemsFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(F_ItemListView.this.getActivity(), "Item Deleted ", Toast.LENGTH_LONG).show();
        }
    }
    //--------------------------------------------------------------------------------------------//

    //--------------------------------------------------------------------------------------------//
    /////////////*************  Show Dialog Box to Confirm Removing   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void showDeleteDialogBox(final Obj_ItemsUsers item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirm Deleting Shopping List ");
        // setup the additional edit text
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new RemoveListDbAsyncTask().execute(item);
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();


    }

    public class itemArrayAdapter  extends ArrayAdapter<Obj_ItemsUsers> {
        private Context context;
        private List<Obj_ItemsUsers> list;

        public itemArrayAdapter(Context context, int simple_list_item_1, List<Obj_ItemsUsers> list) {
            super(context, R.layout.fragment_item_custom_view, list);
            this.context = context;
            this.list = list;
        }

        @Override ////////******** Build List View    ********//////////////
        public View getView(int position, final View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = (View) inflater.inflate(R.layout.fragment_item_custom_view, parent, false);

            TextView tv_itemName  = (TextView) rowView.findViewById(R.id.tv_itemName);
            TextView tv_itemQty  = (TextView) rowView.findViewById(R.id.tv_itemQty);
            TextView tv_itemPrice  = (TextView) rowView.findViewById(R.id.tv_itemPrice);
            TextView tv_itemNotes  = (TextView) rowView.findViewById(R.id.tv_itemNotes);
            //TextView tv_senderName  = (TextView) rowView.findViewById(R.id.tv_senderName);
            ImageView bt_done  = (ImageView) rowView.findViewById(R.id.iv_done);

            //
            selectedItem = list.get(position);
            final Obj_ItemsUsers itemButtonClicked=(Obj_ItemsUsers) selectedItem;
            tv_itemName.setText(String.valueOf(selectedItem.itemName));
            tv_itemQty.setText(String.valueOf(selectedItem.itemQty));
            tv_itemPrice.setText(String.valueOf(selectedItem.itemPrice));
            tv_itemNotes.setText(selectedItem.itemNote);
            //tv_senderName.setText(selectedItem.itemRequester);
            bt_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                // mark Done

                    itemButtonClicked.itemStatus= "done";
                    new MarkdDoneDbAsyncTask().execute(itemButtonClicked);
                    final int listid = list_id;
                    new LoadItemsFromDbAsyncTask().execute(listid);
                }
            });

            return rowView;
        }
    }
    }

