﻿<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


DB::$dbName = 'fsldb';
DB::$user = 'fsldb_user';
DB::$password = "DHhN8gNH";
DB::$host = 'mysql.canadaresources.ca';
DB::$port = 3306;

DB::$encoding = 'utf8';

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';

function error_handler($params) {
    global $log;
    if (isset($params['query'])) {
        $log->error("SQL Error: " . $params['error']);
        $log->error("SQL Query: " . $params['query']);
    } else {
        $log->error("Database Error: " . $params['error']);
    }
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error');
    die; // don't want to keep going if a query broke
}

// this script will always return JSON to any client


$app = new \Slim\Slim();
$app->response()->header('content-type', 'application/json');

// Modify PHP Slim main error handler to produce JSON and write to log
// TO BE TESTED !!!
$app->error(function (\Exception $e) use ($app, $log) {
    $log->error($e);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error (exception)');
});

$app->notFound(function() use ($app) {
    $app->response()->status(404);
    echo json_encode("404 - not found");
});


// returns FALSE if authentication is missing or invalid
// returns users table record of authenticated users if user/pass was correct
//function getAuthUser() {
//    global $app;
//    $token = $charset = $app->request->headers->get('X-Token');
//    $authUser = DB::queryFirstRow("SELECT * FROM users WHERE token=%s", $token);    
//    return $authUser ? $authUser : FALSE;
//}

function getUser() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        return FALSE;
    }
    $email = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];
    $User = DB::queryFirstRow("SELECT * FROM users WHERE userEmail=%s", $email);
    if (!$User) {
        return FALSE;
    }
//    if ($User['password'] === $password) {
//        unset($User['password']);
//        return $User;
//    } else {
//        return FALSE;
//    }
}

//$app->put('/users/:email', function() use ($app) {
//    $json = $app->request()->getBody();
//    $data = json_decode($json, TRUE);
//    $password = $data['password'];
//    $email = $data['userEmail'];
//    $status = "active";
//    // password must be verified in PHP, case sensitive
//    $authUser = DB::queryFirstRow("SELECT * FROM users WHERE userStatus=%s AND userEmail=%s", $status, $email);
//    if (!$authUser) {
//        $app->response()->status(404);
//        echo json_encode("404 - unauthorized (User not found)");
//    } else {
//        if ($authUser['password'] != $password) {
//            $app->response()->status(400);
//            echo json_encode("400 - unauthorized (Invalid login)");
//        } else {
//            echo json_encode($authUser, JSON_PRETTY_PRINT);
//        }
//    }
//});



//$app->get('/users/:token', function($token) use ($app) {
//    $user = DB::queryFirstRow("SELECT * FROM users WHERE token=%i", $token);
//    if ($user) {
//        echo json_encode($user, JSON_PRETTY_PRINT);
//    } else {
//        $app->response()->status(404);
//        echo json_encode("404 - not found for token = " . $token);
//    }
//});


$app->post('/users', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    $userEmail = $data['userEmail'];

    // FIXME: verify user data is valid
    
    // FIXME: Make sure email is not in use yet, return 400 if it is
    $olduseremail = DB::queryFirstRow("SELECT * FROM users WHERE userEmail=%s", $userEmail);
    if ($olduseremail){
        echo json_encode("400 - Email is already exist" . $userEmail);
        $app->response()->status(400);
        return;
    }

    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }

    DB::insert('users', $data);
    $app->response()->status(201);   
    $id=DB::insertId();
    $user = DB::queryFirstRow("SELECT * FROM users WHERE  userId=%i", $id);
    echo json_encode($user, JSON_PRETTY_PRINT);  
});
function isUserValid($user) {
    if (is_null($user)) return "JSON parsing failed, user is null";
    if (!isset($user['userEmail']) || !isset($user['password'])) return "Invalid number of values received";
    if (filter_var($user['userEmail'], FILTER_VALIDATE_EMAIL) === FALSE) return "Email is invalid";
    // TODO: require quality passwords, e.g. one upper-case, one lower-case, one digit or special character
    if (strlen($user['password']) < 6) return "Password too short, must be 6 characters minimum";
    return TRUE;
}

 //login - breaks the rules of RESTFUL
$app->post('/users/login', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    $userPassword = $data['password'];
    $userEmail = $data['userEmail'];

     
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }

    $user = DB::queryFirstRow("SELECT * FROM users WHERE userEmail=%s", $userEmail);
    if (!$user){
        echo json_encode("405 - Invalid user") ;
        $app->response()->status(405);
        return;
    }
    if ($user['password'] !== $userPassword){
        echo json_encode("400 - Invalid authontication ") ;
        $app->response()->status(400);
        return;
    }
    echo json_encode($user, JSON_PRETTY_PRINT);  
    
});

$app->delete('/users/:token', function($token) {
    DB::delete('users', 'token=%d', $token);
    echo json_encode(DB::affectedRows() != 0);
});
//Post  new Circle 
$app->post('/circles/:token', function($token) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    
    $user = DB::queryFirstRow("SELECT * FROM users WHERE token=%s", $token);
    if (!$user){
        echo json_encode("400 - Invalid user token" . $token);
        $app->response()->status(400);
        return;
    }
    $userId = $user['userId'];
    $data['circleAdminId']= $userId;
    DB::insert('circles', $data);
    $app->response()->status(201);   
    $id=DB::insertId();
    $user = DB::queryFirstRow("SELECT * FROM circles WHERE circleId=%s", $id); 
    echo json_encode($user, JSON_PRETTY_PRINT);  
});

//$app->get('/circles', function() use ($app, $log) {
//    $authUser = getAuthUser();
//    if ($authUser === FALSE) {
//        $app->response()->status(401);
//        echo json_encode("401 - unauthorized (authentication missing or invalid)" );
//    }
//    $circlesList = DB::query("SELECT * FROM circles WHERE userId=%i", $authUser['userId']);
//    echo json_encode($circlesList, JSON_PRETTY_PRINT);
//});

$app->run();

