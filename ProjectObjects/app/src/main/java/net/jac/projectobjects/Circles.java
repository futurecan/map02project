package net.jac.projectobjects;

public class Circles {

    int circleId ;
    String circleName ;
    int circleAdminId ;
    String circleDescription ;
    CircleStatus isActive ;
    enum CircleStatus {active , disabled} ;

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", circleId, circleAdminId, circleName , circleDescription , isActive);
    }

    public Circles(int circleId, String circleName, int circleAdminId, String circleDescription, CircleStatus isActive) {
        this.circleId = circleId;
        this.circleName = circleName;
        this.circleAdminId = circleAdminId;
        this.circleDescription = circleDescription;
        this.isActive = isActive;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public int getCircleAdminId() {
        return circleAdminId;
    }

    public void setCircleAdminId(int circleAdminId) {
        this.circleAdminId = circleAdminId;
    }

    public String getCircleDescription() {
        return circleDescription;
    }

    public void setCircleDescription(String circleDescription) {
        this.circleDescription = circleDescription;
    }

    public CircleStatus getIsActive() {
        return isActive;
    }

    public void setIsActive(CircleStatus isActive) {
        this.isActive = isActive;
    }
}
