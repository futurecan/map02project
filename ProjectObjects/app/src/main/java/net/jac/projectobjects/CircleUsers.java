package net.jac.projectobjects;

public class CircleUsers {

    int circleUsersId ;
    CircleUserStatus pending ;
    enum CircleUserStatus {pending , accepted , rejected}

    public CircleUsers(int circleUsersId, CircleUserStatus pending) {
        this.circleUsersId = circleUsersId;
        this.pending = pending;
    }

    public int getCircleUsersId() {
        return circleUsersId;
    }

    public void setCircleUsersId(int circleUsersId) {
        this.circleUsersId = circleUsersId;
    }

    public CircleUserStatus getPending() {
        return pending;
    }

    public void setPending(CircleUserStatus pending) {
        this.pending = pending;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", circleUsersId, pending);
    }
}
