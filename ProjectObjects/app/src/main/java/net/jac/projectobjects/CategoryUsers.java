package net.jac.projectobjects;

public class CategoryUsers {

    int catId ;
    String catName ;
    int catAdminId ;

    public CategoryUsers(int catId, String catName, int catAdminId) {
        this.catId = catId;
        this.catName = catName;
        this.catAdminId = catAdminId;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", catId, catName, catAdminId);
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatAdminId() {
        return catAdminId;
    }

    public void setCatAdminId(int catAdminId) {
        this.catAdminId = catAdminId;
    }
}
