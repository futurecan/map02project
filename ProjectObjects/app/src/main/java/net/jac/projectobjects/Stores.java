package net.jac.projectobjects;

public class Stores {

    int storeId ;
    String storeName ;

    public Stores(int storeId, String storeName) {
        this.storeId = storeId;
        this.storeName = storeName;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", storeId, storeName);
    }
}
