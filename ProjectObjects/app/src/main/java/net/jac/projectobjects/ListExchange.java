package net.jac.projectobjects;

public class ListExchange {

    int listId ;
    int listIncoming ;
    int listOutgoing ;
    ListStatus pending ;
    enum ListStatus {pending , accepted, rejected}

    public ListExchange(int listId, int listIncoming, int listOutgoing, ListStatus pending) {
        this.listId = listId;
        this.listIncoming = listIncoming;
        this.listOutgoing = listOutgoing;
        this.pending = pending;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getListIncoming() {
        return listIncoming;
    }

    public void setListIncoming(int listIncoming) {
        this.listIncoming = listIncoming;
    }

    public int getListOutgoing() {
        return listOutgoing;
    }

    public void setListOutgoing(int listOutgoing) {
        this.listOutgoing = listOutgoing;
    }

    public ListStatus getPending() {
        return pending;
    }

    public void setPending(ListStatus pending) {
        this.pending = pending;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", listId, listIncoming, listOutgoing , pending);
    }
}
