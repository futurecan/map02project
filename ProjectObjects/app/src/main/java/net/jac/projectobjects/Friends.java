package net.jac.projectobjects;

public class Friends {
    int connectId ;
    int userAId ;
    int userBId ;

    public Friends(int connectId, int userAId, int userBId) {
        this.connectId = connectId;
        this.userAId = userAId;
        this.userBId = userBId;
    }

    public int getConnectId() {
        return connectId;
    }

    public void setConnectId(int connectId) {
        this.connectId = connectId;
    }

    public int getUserAId() {
        return userAId;
    }

    public void setUserAId(int userAId) {
        this.userAId = userAId;
    }

    public int getUserBId() {
        return userBId;
    }

    public void setUserBId(int userBId) {
        this.userBId = userBId;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", connectId, userAId, userBId);
    }
}

