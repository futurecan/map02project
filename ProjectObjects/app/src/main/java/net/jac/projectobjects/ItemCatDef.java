package net.jac.projectobjects;

public class ItemCatDef {

    int catId ;
    String catName;

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public ItemCatDef(int catId, String catName) {

        this.catId = catId;
        this.catName = catName;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", catId, catName);
    }
}
