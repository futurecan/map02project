package com.futurecan.arrays;

public class Obj_ItemCategoriesDefault {

    int catId ;
    String catName;

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Obj_ItemCategoriesDefault(int catId, String catName) {

        this.catId = catId;
        this.catName = catName;
    }

    public Obj_ItemCategoriesDefault() {
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", catId, catName);
    }
}
