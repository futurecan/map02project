package com.futurecan.arrays;

public class Obj_ListsUsers {
    int listId;
    int listAdmin ;
    String listName ;
    String listNote ;
    ListActive active ;
    enum ListActive {active , inactive }
    ListSaved saved ;
    enum ListSaved {saved , unsaved }

    public Obj_ListsUsers(int listId, int listAdmin, String listName, String listNote, ListActive active, ListSaved saved) {
        this.listId = listId;
        this.listAdmin = listAdmin;
        this.listName = listName;
        this.listNote = listNote;
        this.active = active;
        this.saved = saved;
    }

    public Obj_ListsUsers() {
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getListAdmin() {
        return listAdmin;
    }

    public void setListAdmin(int listAdmin) {
        this.listAdmin = listAdmin;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListNote() {
        return listNote;
    }

    public void setListNote(String listNote) {
        this.listNote = listNote;
    }

    public ListActive getActive() {
        return active;
    }

    public void setActive(ListActive active) {
        this.active = active;
    }

    public ListSaved getSaved() {
        return saved;
    }

    public void setSaved(ListSaved saved) {
        this.saved = saved;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", listId, listAdmin, listName , listNote, active , saved);
    }
}
