package com.futurecan.arrays;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Obj_ItemsUsers {

    int itemUserid ;
    int itemId ;
    String itemName ;
    String itemNote ;
    int itemCatId ;
    int listId ;
    ItemStatus pending ;
    enum ItemStatus{ pending , done , rejected}
    Date itemDoneDate ;
    Double itemPrice ;
    int itemStore ;
    int itemRequester ;
    int itemQty ;

    public Obj_ItemsUsers(int itemUserid, int itemId, String itemName, String itemNote, int itemCatId, int listId, ItemStatus pending, Date itemDoneDate, Double itemPrice, int itemStore, int itemRequester, int itemQty) {
        this.itemUserid = itemUserid;
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemNote = itemNote;
        this.itemCatId = itemCatId;
        this.listId = listId;
        this.pending = pending;
        this.itemDoneDate = itemDoneDate;
        this.itemPrice = itemPrice;
        this.itemStore = itemStore;
        this.itemRequester = itemRequester;
        this.itemQty = itemQty;
    }

    public Obj_ItemsUsers() {
    }

    public int getItemUserid() {
        return itemUserid;
    }

    public void setItemUserid(int itemUserid) {
        this.itemUserid = itemUserid;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNote() {
        return itemNote;
    }

    public void setItemNote(String itemNote) {
        this.itemNote = itemNote;
    }

    public int getItemCatId() {
        return itemCatId;
    }

    public void setItemCatId(int itemCatId) {
        this.itemCatId = itemCatId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public ItemStatus getPending() {
        return pending;
    }

    public void setPending(ItemStatus pending) {
        this.pending = pending;
    }

    public Date getItemDoneDate() {
        return itemDoneDate;
    }

    public void setItemDoneDate(Date itemDoneDate) {
        this.itemDoneDate = itemDoneDate;
    }

    public Double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemStore() {
        return itemStore;
    }

    public void setItemStore(int itemStore) {
        this.itemStore = itemStore;
    }

    public int getItemRequester() {
        return itemRequester;
    }

    public void setItemRequester(int itemRequester) {
        this.itemRequester = itemRequester;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    @Override
    public String toString() {
        String doneDate = Z_Globals.dateFormatLocal.format(itemDoneDate);
        return String.format("%d: %s due on %s (%s)", itemUserid, itemId, doneDate, itemName, itemNote, itemCatId, listId, pending, itemPrice,itemStore,itemRequester,itemQty);
    }

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
}
