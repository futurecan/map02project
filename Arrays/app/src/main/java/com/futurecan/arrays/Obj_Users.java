package com.futurecan.arrays;

import java.util.Date;

public class Obj_Users {

    int userId ;
    String userName;
    String userEmail ;
    String userToken ;
    Date  userLastLogin ;
    UserStatus active ;
    enum UserStatus {active , disabled , reserPassword}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public Date getUserLastLogin() {
        return userLastLogin;
    }

    public void setUserLastLogin(Date userLastLogin) {
        this.userLastLogin = userLastLogin;
    }

    public UserStatus getActive() {
        return active;
    }

    public void setActive(UserStatus active) {
        this.active = active;
    }

    public Obj_Users(int userId, String userName, String userEmail, String userToken, Date userLastLogin, UserStatus active) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userToken = userToken;
        this.userLastLogin = userLastLogin;
        this.active = active;
    }

    public Obj_Users() {
    }

    @Override
    public String toString() {
        String loginDate = Z_Globals.dateFormatLocal.format(userLastLogin);
        return String.format("%d: %s due on %s (%s)", userId, userEmail,userName, userToken, loginDate,active);
    }
}
