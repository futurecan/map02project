package com.futurecan.arrays;

public class Obj_Notifications {

    int notifId ;
    int notifToUser ;
    int notifFromUser ;
    NotifType circle  ;
    enum NotifType {list , item , friend , circle}
    NotifStatus pending ;
    enum NotifStatus {pending , done }
    int notifTypeId ;

    public Obj_Notifications(int notifId, int notifToUser, int notifFromUser, NotifType circle, NotifStatus pending, int notifTypeId) {
        this.notifId = notifId;
        this.notifToUser = notifToUser;
        this.notifFromUser = notifFromUser;
        this.circle = circle;
        this.pending = pending;
        this.notifTypeId = notifTypeId;
    }

    public Obj_Notifications() {
    }

    public int getNotifId() {
        return notifId;
    }

    public void setNotifId(int notifId) {
        this.notifId = notifId;
    }

    public int getNotifToUser() {
        return notifToUser;
    }

    public void setNotifToUser(int notifToUser) {
        this.notifToUser = notifToUser;
    }

    public int getNotifFromUser() {
        return notifFromUser;
    }

    public void setNotifFromUser(int notifFromUser) {
        this.notifFromUser = notifFromUser;
    }

    public NotifType getCircle() {
        return circle;
    }

    public void setCircle(NotifType circle) {
        this.circle = circle;
    }

    public NotifStatus getPending() {
        return pending;
    }

    public void setPending(NotifStatus pending) {
        this.pending = pending;
    }

    public int getNotifTypeId() {
        return notifTypeId;
    }

    public void setNotifTypeId(int notifTypeId) {
        this.notifTypeId = notifTypeId;
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", notifId, notifToUser, notifFromUser , circle, pending , notifTypeId);
    }
}
