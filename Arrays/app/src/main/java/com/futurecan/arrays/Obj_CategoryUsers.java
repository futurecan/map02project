package com.futurecan.arrays;

public class Obj_CategoryUsers {

    int catId ;
    String catName ;
    int catAdminId ;

    public Obj_CategoryUsers(int catId, String catName, int catAdminId) {
        this.catId = catId;
        this.catName = catName;
        this.catAdminId = catAdminId;
    }

    public Obj_CategoryUsers() {
    }

    @Override
    public String toString() {
        return String.format("%d: %s due on %s (%s)", catId, catName, catAdminId);
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatAdminId() {
        return catAdminId;
    }

    public void setCatAdminId(int catAdminId) {
        this.catAdminId = catAdminId;
    }
}
