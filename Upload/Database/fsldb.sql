-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql.canadaresources.ca
-- Generation Time: Dec 21, 2018 at 01:43 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fsldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `categoriesUsers`
--

CREATE TABLE `categoriesUsers` (
  `categoryId` bigint(11) NOT NULL,
  `categoryName` varchar(50) NOT NULL,
  `categoryAdmin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `circles`
--

CREATE TABLE `circles` (
  `circleId` int(11) NOT NULL,
  `circleName` varchar(50) NOT NULL,
  `circleAdminId` int(11) NOT NULL,
  `circleDescriptipn` varchar(150) NOT NULL,
  `circleStatus` enum('active','disabled') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `circleUsers`
--

CREATE TABLE `circleUsers` (
  `id` int(11) NOT NULL,
  `circleUserId` int(11) NOT NULL,
  `circleUserStatus` enum('pinding','accepted','rejected') NOT NULL,
  `circleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `connectId` int(11) NOT NULL,
  `userAid` int(11) NOT NULL,
  `userBid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `itemCategoriesDefault`
--

CREATE TABLE `itemCategoriesDefault` (
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `itemsDefault`
--

CREATE TABLE `itemsDefault` (
  `itemId` int(11) NOT NULL,
  `itemName` varchar(50) NOT NULL,
  `itemDescription` varchar(100) NOT NULL,
  `itemCategory` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `itemsUsers`
--

CREATE TABLE `itemsUsers` (
  `id` bigint(20) NOT NULL,
  `itemId` bigint(20) NOT NULL,
  `itemName` varchar(50) NOT NULL,
  `itemNote` varchar(150) NOT NULL,
  `itemCategoryId` int(11) NOT NULL,
  `listId` bigint(20) NOT NULL,
  `itemStatus` enum('pending','done','rejected') NOT NULL,
  `itemDoneDate` date NOT NULL,
  `itemPrice` decimal(10,0) NOT NULL,
  `itemStore` bigint(20) NOT NULL,
  `itemRequester` int(11) NOT NULL,
  `itemQty` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `listExchange`
--

CREATE TABLE `listExchange` (
  `listId` bigint(20) NOT NULL,
  `listIncomingUser` int(11) NOT NULL,
  `listOutgoingUser` int(11) NOT NULL,
  `listStatus` enum('pending','accepted','rejected') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `listsUsers`
--

CREATE TABLE `listsUsers` (
  `listId` bigint(20) NOT NULL,
  `listAdmin` int(11) NOT NULL,
  `listName` varchar(50) NOT NULL,
  `listNote` varchar(100) NOT NULL,
  `listActive` enum('yes','no') NOT NULL,
  `listSaved` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notificationId` bigint(11) NOT NULL,
  `notificationToUser` int(11) NOT NULL,
  `notificationFromUser` int(11) NOT NULL,
  `notificationType` enum('list','item','friend','circle') NOT NULL,
  `notificationStatus` enum('pending','done') NOT NULL,
  `notificationTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `storeId` bigint(20) NOT NULL,
  `storeName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(25) NOT NULL,
  `userEmail` varchar(100) NOT NULL,
  `token` varchar(50) NOT NULL,
  `userLastLogin` date NOT NULL,
  `userStatus` enum('active','disabled','resetPassword') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userName`, `password`, `userEmail`, `token`, `userLastLogin`, `userStatus`) VALUES
(1, 'Jerry1', 'abcd12345', 'harry1@harry1.com', 'kgsKJVGKJBG56465', '2008-11-11', 'active'),
(2, 'Remon', '12345678', 'r.setup@yahoo.com', 'kgsKJVGKJBG57777', '2008-11-11', 'active'),
(4, 'Nancy', '12345678', 'nancy@gmail.com', '54cdc7574396767e5d25b5746b97a660', '2008-11-11', 'active'),
(1000, 'Nancy6', '12345678', 'nancy66@gmail.com', '54cdc7574396767e5d25b5746b97a660', '2008-11-11', 'active'),
(1001, 'Nancy699', '12345678', 'nancy6699@gmail.com', '54cdc7574396767e5d25b5746b97a660', '2008-11-11', 'active'),
(1002, 'mama', '1256789', 'mama', '269389bb4c1c4aa9d913e8b2663a', '2000-11-11', 'active'),
(1003, 'sherif', '123456', 'sherif', '18bb25483debb7f97355218417d1dfa4', '2000-11-11', 'active'),
(1004, 'Pepo', '123456', 'Pepo', 'c45171f8818162128562ffeca72638f', '2000-11-11', 'active'),
(1005, 'sherif5', '12345678', 'sherif5', '1dc821e1076e6b840dab1eff32a770', '2000-11-11', 'active'),
(1006, 'Emad', '123466', 'emad@emad.com', 'f87e2babff882e7154cbcc4fc7814d2', '2000-11-11', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoriesUsers`
--
ALTER TABLE `categoriesUsers`
  ADD KEY `user` (`categoryAdmin`);

--
-- Indexes for table `circles`
--
ALTER TABLE `circles`
  ADD PRIMARY KEY (`circleId`),
  ADD KEY `circleAdminId` (`circleAdminId`);

--
-- Indexes for table `circleUsers`
--
ALTER TABLE `circleUsers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`circleUserId`),
  ADD KEY `circleId` (`circleId`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`connectId`),
  ADD KEY `userAid` (`userAid`),
  ADD KEY `userBid` (`userBid`);

--
-- Indexes for table `itemCategoriesDefault`
--
ALTER TABLE `itemCategoriesDefault`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `itemsDefault`
--
ALTER TABLE `itemsDefault`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `itemsUsers`
--
ALTER TABLE `itemsUsers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `itemRequester` (`itemRequester`),
  ADD KEY `listId` (`listId`);

--
-- Indexes for table `listExchange`
--
ALTER TABLE `listExchange`
  ADD KEY `listId` (`listId`),
  ADD KEY `listIncomingUser` (`listIncomingUser`),
  ADD KEY `listOutgoingUser` (`listOutgoingUser`);

--
-- Indexes for table `listsUsers`
--
ALTER TABLE `listsUsers`
  ADD PRIMARY KEY (`listId`),
  ADD KEY `listAdmin` (`listAdmin`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notificationId`),
  ADD KEY `notificationFromUser` (`notificationFromUser`),
  ADD KEY `notificationToUser` (`notificationToUser`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`storeId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `circles`
--
ALTER TABLE `circles`
  MODIFY `circleId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `circleUsers`
--
ALTER TABLE `circleUsers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `connectId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itemCategoriesDefault`
--
ALTER TABLE `itemCategoriesDefault`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itemsDefault`
--
ALTER TABLE `itemsDefault`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itemsUsers`
--
ALTER TABLE `itemsUsers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listsUsers`
--
ALTER TABLE `listsUsers`
  MODIFY `listId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notificationId` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `storeId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categoriesUsers`
--
ALTER TABLE `categoriesUsers`
  ADD CONSTRAINT `categoriesUsers_ibfk_1` FOREIGN KEY (`categoryAdmin`) REFERENCES `users` (`userId`);

--
-- Constraints for table `circles`
--
ALTER TABLE `circles`
  ADD CONSTRAINT `circles_ibfk_1` FOREIGN KEY (`circleAdminId`) REFERENCES `users` (`userId`);

--
-- Constraints for table `circleUsers`
--
ALTER TABLE `circleUsers`
  ADD CONSTRAINT `circleUsers_ibfk_1` FOREIGN KEY (`circleUserId`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `circleUsers_ibfk_2` FOREIGN KEY (`circleId`) REFERENCES `circles` (`circleId`);

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`userAid`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`userBid`) REFERENCES `users` (`userId`);

--
-- Constraints for table `itemsUsers`
--
ALTER TABLE `itemsUsers`
  ADD CONSTRAINT `itemsUsers_ibfk_1` FOREIGN KEY (`itemRequester`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `itemsUsers_ibfk_2` FOREIGN KEY (`listId`) REFERENCES `listsUsers` (`listId`);

--
-- Constraints for table `listExchange`
--
ALTER TABLE `listExchange`
  ADD CONSTRAINT `listExchange_ibfk_1` FOREIGN KEY (`listId`) REFERENCES `listsUsers` (`listId`),
  ADD CONSTRAINT `listExchange_ibfk_2` FOREIGN KEY (`listIncomingUser`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `listExchange_ibfk_3` FOREIGN KEY (`listOutgoingUser`) REFERENCES `users` (`userId`);

--
-- Constraints for table `listsUsers`
--
ALTER TABLE `listsUsers`
  ADD CONSTRAINT `listsUsers_ibfk_1` FOREIGN KEY (`listAdmin`) REFERENCES `users` (`userId`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`notificationFromUser`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`notificationToUser`) REFERENCES `users` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
