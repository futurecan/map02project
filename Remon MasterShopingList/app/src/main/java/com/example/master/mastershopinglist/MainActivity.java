package com.example.master.mastershopinglist;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DailogClass.DailogClassListener {

    private static final String DATABASE_NAME = "items_db";
    private AppDatabase appDatabase;
    public final static String TAG = "MainActivity";
    public Item selectedItem;
    ListView lvitems;
    List<Item> itemsList;
    ArrayAdapter<Item> itemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();
        lvitems = (ListView) findViewById(R.id.lvItems);

        itemsList = new ArrayList<>();
        itemsAdapter = new itemsArrayAdapter(this,  itemsList);
        lvitems.setAdapter(itemsAdapter);
    }
    @Override
    protected void onStart() {
        super.onStart();
        new LoadItemsFromDbAsyncTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        openDialog();
                return true;
    }
    //--------------------------------------------------------------------------------------------//
         /////////////************* Call Build  Dialog Box Class   ***************////////////
    //--------------------------------------------------------------------------------------------//
    private void openDialog(){
        DailogClass mydialog = new DailogClass();
        mydialog.show(getSupportFragmentManager(),"Add New Item");
}
    //--------------------------------------------------------------------------------------------//
    /////////////************* Get Returned Value from Dialog Box Class   ***************////////////
    //--------------------------------------------------------------------------------------------//
@Override
    public void applyText(String itemName, String itemDesc) {
        new InsertItemDbAsyncTask().execute(new Item(0, itemName,itemDesc));
        Toast.makeText(MainActivity.this, "Item added successfuly" , Toast.LENGTH_SHORT).show();
        new LoadItemsFromDbAsyncTask().execute(); // reload the list
    }

    //--------------------------------------------------------------------------------------------//
    /////////////************* Insert into Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class InsertItemDbAsyncTask extends AsyncTask<Item, Void, Void> {

        @Override
        protected Void doInBackground(Item... params) {
            Item item = params[0];
            appDatabase.daoAccess().insertItem(item);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            new LoadItemsFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(MainActivity.this, "Item added", Toast.LENGTH_LONG).show();
        }

    }
    class RemoveItemDbAsyncTask extends AsyncTask<Item, Void, Void> {

        @Override
        protected Void doInBackground(Item... params) {
            Item item = params[0];
            appDatabase.daoAccess().deleteItem(item);
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return null;
        }

        @Override
        protected void onPostExecute(Void list) {
            new LoadItemsFromDbAsyncTask().execute(); // reload the list
            Toast.makeText(MainActivity.this, "Item Done", Toast.LENGTH_LONG).show();
        }

    }
    //--------------------------------------------------------------------------------------------//
    /////////////*************  Load from Database By AsyncTask Thread   ***************////////////
    //--------------------------------------------------------------------------------------------//
    class LoadItemsFromDbAsyncTask extends AsyncTask<Void, Void, List<Item>> {

        @Override
        protected List<Item> doInBackground(Void... params) {
            List<Item> list = appDatabase.daoAccess().fetchAllItems();
            // FIXME: Find out what exceptions to expect, catch it and return null in such case
            return list;
        }

        @Override
        protected void onPostExecute(List<Item> list) {
            if (list == null) {
                Toast.makeText(MainActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                itemsList.clear();
                for (Item item : list) {
                    itemsList.add(item);
                }
                itemsAdapter.notifyDataSetChanged();
            }
        }

    }

        public class itemsArrayAdapter  extends ArrayAdapter<Item> {
            private Context context;
            private List<Item> list;

            public itemsArrayAdapter(Context context, List<Item> list) {
                super(context, R.layout.item_list_layout, list);
                this.context = context;
                this.list = list;
            }

            @Override ////////******** Build List View    ********//////////////
            public View getView(int position, final View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                // FIXME: reuse convert view if available
                View rowView = inflater.inflate(R.layout.item_list_layout, parent, false);
                TextView tvFirstLine = (TextView) rowView.findViewById(R.id.tvFirstLine);
                TextView tvSecondLine = (TextView) rowView.findViewById(R.id.tvSecondLine);
                Button btDone = (Button) rowView.findViewById(R.id.btDone);
                //
                selectedItem = list.get(position);
                final Item itemButtonClicked=(Item)selectedItem;
                tvFirstLine.setText(selectedItem.itemName);
                tvSecondLine.setText(selectedItem.note);
                btDone.setFocusable(false);
                btDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        new RemoveItemDbAsyncTask().execute(itemButtonClicked);
                    }

                });
                //
                return rowView;
            }
    }

}

