package com.example.master.mastershopinglist;
import android.arch.persistence.room.*;

@Entity(tableName = "Items")
public class Item {
  @PrimaryKey(autoGenerate = true)
 private int itemid;
  String itemName;
  String note;

    public Item() {
    }

    public Item(int itemid, String itemName, String note) {
        this.itemid = itemid;
        this.itemName = itemName;
        this.note = note;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return String.format("%d: %s", itemName, note);
    }
}
