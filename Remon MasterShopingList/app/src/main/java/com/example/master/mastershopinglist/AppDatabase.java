package com.example.master.mastershopinglist;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.content.ClipData;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DaoAccess daoAccess();
}

