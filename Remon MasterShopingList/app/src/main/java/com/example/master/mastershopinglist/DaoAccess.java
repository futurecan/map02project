package com.example.master.mastershopinglist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    void insertItem(Item item);

    @Insert
    void insertMultipleItems(List<Item> ItemsList);

    @Query("SELECT * FROM Items WHERE itemid = :itemid")
    Item fetchItemById(int itemid);

    @Query("SELECT * FROM Items")
    List<Item> fetchAllItems();

    @Update
    void updateItem(Item item);

    @Delete
    void deleteItem(Item item);

}