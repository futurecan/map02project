package com.example.master.mastershopinglist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.ContentFrameLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DailogClass extends AppCompatDialogFragment {
    private EditText etItemName;
    private EditText etItemDesc;
    private DailogClassListener listener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view =inflater.inflate(R.layout.new_item_dialog,null);
        builder.setView(view);
        builder.setTitle("Add new item");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

            }
        });
        builder.setPositiveButton("OK",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int i) {
            String itemName = etItemName.getText().toString();
            String itemDesc = etItemDesc.getText().toString();
            listener.applyText(itemName,itemDesc);
            }
        });
        etItemName = view.findViewById(R.id.tvItem);
        etItemDesc= view.findViewById(R.id.tvDescription);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (DailogClassListener)context;
        } catch (ClassCastException e) {
                throw new ClassCastException(context.toString() + "Must impelement DailogClassListener ");
        }
    }

    public interface DailogClassListener{
        void applyText(String itemName, String itemDesc);
    }


}
